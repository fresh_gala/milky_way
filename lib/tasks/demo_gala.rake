class DemoGala
  def initialize
    start_creating
  end

  private

  def start_creating
    create_masters
    insert_users_and_locations
    insert_vendors_and_locations
    start_creating_subscriptions
  end

  def create_masters
    product_cats = YAML::load_file(File.join(Rails.root, 'test', 'fixtures', 'product_categories.yml'))
    ProductCategory.import(product_cats.values, validate: false)
    products = YAML::load_file(File.join(Rails.root, 'test', 'fixtures', 'products.yml'))
    Product.import(products.values.map {|d| d.merge('product_category_id' => find_category(d['product_category']).id).except('product_category')}, validate: false)
  end

  def find_category(name)
    ProductCategory.find_by_name name.titleize
  end

  def start_creating_subscriptions
    puts 'Creating subscriptions'
    VendorProduct.all.each.with_index do |vp, _|
      ['Sunil', 'Parthi', 'Vignesh'].each do |name|
        user = find_user(name)
        subscription = user.subscriptions.create!(
            start_on: Date.tomorrow,
            frequency: 'Every Day',
            vendor_product: vp,
            quantity: 1,
            user_location: user.locations.sample
        )
        next if subscription.errors.present?
        CashPayment.create(user, '2pm to 6pm', subscription)
      end
    end
    CashPayment.all.each(&:paid!)
  end

  def insert_vendors_and_locations
    puts 'Creating vendors'
    vendor_data = JSON.parse(File.read("#{Rails.root}/lib/tasks/demo_data/vendors.json"))
    Vendor.import(vendor_data.map {|d| d.merge("encrypted_password" => Devise::Encryptor.digest(Vendor, 'password'))})
    puts 'Creating vendor locations'
    location_data = JSON.parse(File.read("#{Rails.root}/lib/tasks/demo_data/vendor_locations.json"))
    location_data = location_data.map {|d| d.merge('vendor_id' => find_by_name(Vendor, d['vendor']).id).except('vendor')}
    VendorLocation.import(location_data)
    puts 'Creating vendor products'
    product_data = JSON.parse(File.read("#{Rails.root}/lib/tasks/demo_data/vendor_products.json"))
    product_data = product_data.map do |d|
      d.merge(
          product_id: find_by_name(Product, d['product']).id,
          vendor_id: find_by_name(Vendor, d['vendor']).id
      ).except('product', 'vendor')
    end
    VendorProduct.import(product_data)
  end

  def insert_users_and_locations
    puts 'Creating users'
    user_data = JSON.parse(File.read("#{Rails.root}/lib/tasks/demo_data/users.json"))
    User.create(user_data.map {|d| d.merge("password_digest" => BCrypt::Password.create('password'))})
    puts 'Creating users locations'
    location_data = JSON.parse(File.read("#{Rails.root}/lib/tasks/demo_data/user_locations.json"))
    location_data = location_data.map {|d| d.merge('user_id' => find_by_name(User, d['user']).id).except('user')}
    UserLocation.import(location_data)
  end

  def find_by_name(klass, name)
    klass.find_by_name name
  end

  def find_user(name = 'Sunil')
    find_by_name(User, "User #{name}")
  end

end


namespace :demo_gala do
  task create: :environment do
    puts 'Deleting models...'
    start_time = Time.zone.now
    dependencies = %w(PaperTrail::Version
                    DailyUsage
                    PaymentReceiptItem
                    CashPayment
                    PaymentReceipt PaymentToken Payment
                    SubscriptionPeriod Subscription
                    VendorLocation VendorProduct Vendor CartItem Cart
                    UserTransaction Wallet UserLocation User Product ProductCategory)
    ApplicationRecord.transaction do
      dependencies.each do |dependency|
        dependency.constantize.delete_all
      end
    end
    puts 'Start creating galas'
    DemoGala.new
    end_time = Time.zone.now
    time_taken = ((end_time - start_time) / 1.minute).round
    puts "Time taken for this task is #{time_taken} minutes 😁"
  end
end
