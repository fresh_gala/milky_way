namespace :daily_usages do
  task create: :environment do
    Subscription.active.each do |subscription|
      subscription.create_period(Date.today.next_month)
    end
  end
end
