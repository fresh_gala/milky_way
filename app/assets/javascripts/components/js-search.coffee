$(document).on 'keydown keypress keyup', '[data-behaviour=search-table]', ->
  $this = $(this)
  searchParam = $this.val()
  $searchTargets = $('[data-search-target]')
  if searchParam is ''
    $searchTargets.show()
  else
    $searchTargets.hide()
    $searchTargets.each ->
      $target = $(this)
      if $target.attr('data-search-target').toLowerCase().search(searchParam.toLowerCase()) >= 0
        $target.show()
