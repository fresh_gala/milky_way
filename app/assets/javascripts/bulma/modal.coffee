$(document).on('click', '[data-toggle=modal]', (e)->
  e.preventDefault()
  $this = $(this)
  $target = $($this.data('target'))
  templateId = $this.data('loadTemplate')
  if templateId isnt undefined
    $target.find('.modal-card').html $(templateId).html()
  $($target).addClass('is-active')
)

$(document).on('click', '[data-dismiss=modal], .modal-background', (e) ->
  e.preventDefault()
  $this = $(this)
  $this.parents('.modal').removeClass('is-active')
)