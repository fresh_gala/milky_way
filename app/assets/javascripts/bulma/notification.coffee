$(document).on('click', '[data-dismiss=notification]', ->
  $this = $(this)
  $this.parents('.notification').fadeOut()
)