$(document).on 'change', 'input[type="file"]', (ev) ->
  $cf = $(this).parents('div.file')
  if $cf.find('span.file-name').length > 0
    $cf.find('span.file-name').text ev.target.files[0].name
  else
    $cf.find('span.file-label').text ev.target.files[0].name
