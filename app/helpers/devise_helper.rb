module DeviseHelper

  def devise_error_messages!
    return "" if resource.errors.empty?
    messages = resource.errors.full_messages.join(', ')
    javascript_tag "toastr['warning']('#{messages}')"
  end

end
