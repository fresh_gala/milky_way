module ApplicationHelper

  def get_flash_key key
    return 'warning' if key == 'alert'
    return 'warning' if key == 'notice'
    key
  end

  def ldt date
    date&.strftime '%d %b %Y'
  end

  def ldt_with_time date
    date&.strftime '%d %b %Y %H:%M:%S %p'
  end

  def address_splitter address
    return if address.nil? || address.size.zero?
    address.map do |add|
      content_tag(:div, add)
    end.join('').html_safe
  end

  def currency_symbol
    '&#x20B9;'.html_safe
  end

end
