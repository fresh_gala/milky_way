module VendorHelper

  def vendor_dashboard_links
    [
        {
            path: vendors_users_path,
            name: 'Users',
            icon: 'users'
        },
        {
            path: vendors_deliveries_path,
            name: 'Deliveries',
            icon: 'truck'
        },
        {
            path: '#',
            name: 'Delivery Agents',
            icon: 'motorcycle'
        },
        {
            path: '#',
            name: 'Products',
            icon: 'list-ul'
        },
    ].map {|s| OpenStruct.new(s)}
  end

end
