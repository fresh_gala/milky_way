module AdminHelper

  def dashboard_links
    [
        {
            name: 'Product Categories',
            path: product_categories_path,
            count: ProductCategory.count
        },
        {
            name: 'Products',
            path: products_path,
            count: Product.count
        },
        {
            name: 'Cash Payments',
            path: cash_payments_path,
            count: CashPayment.unpaid.count
        },
        {
            name: 'Payments',
            path: payments_path,
            count: Payment.count
        },
        {
            name: 'Tickets',
            path: tickets_path,
            count: Ticket.count
        },
        {
            name: 'Subscription Requests',
            path: subscription_request_path,
            count: SubscriptionRequest.count
        },
    ]
  end

  def cash_payments_link
    [
        {
            name: 'Unpaid Payments',
            path: cash_payments_path
        },
        {
            name: 'All Payments',
            path: all_cash_payments_path
        }
    ]
  end

end
