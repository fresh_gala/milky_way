class SendPaymentReceiptWorker
  include Sidekiq::Worker

  def perform(payment_receipt_id)
    @payment_receipt = PaymentReceipt.find_by id: payment_receipt_id
    PaymentReceiptMailer.send_receipt(@payment_receipt).deliver_now! if Rails.env.production?
  end
end
