class TicketAlertWorker
  include Sidekiq::Worker

  def perform(ticket_id)
    @ticket = Ticket.find ticket_id
    TicketMailer.welcome_alert(@ticket).deliver_now!
    AdminMailer.ticket_create_alert(@ticket).deliver_now!
  end
end
