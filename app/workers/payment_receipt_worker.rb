class PaymentReceiptWorker
  include Sidekiq::Worker

  def perform(payment_id)
    @payment = Payment.find(payment_id)
    Payment::Receipt.new(@payment).create
  end

end
