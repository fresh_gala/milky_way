class SendOtpWorker
  include Sidekiq::Worker
  sidekiq_options queue: 'critical'

  def perform(user_id, otp, otp_for = 'User')
    user = otp_for.constantize.find(user_id)
    if Rails.env.in?(['test', 'development'])
      puts otp
    else
      Messaging.send(user.mobile, "This is a OTP to verify your mobile number #{otp}") if Rails.env != 'test'
    end
  end
end
