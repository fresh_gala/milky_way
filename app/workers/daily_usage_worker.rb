class DailyUsageWorker
  include Sidekiq::Worker
  sidekiq_options queue: 'critical'

  def perform(sp_id)
    @subscription_period = SubscriptionPeriod.find(sp_id)
    @subscription = @subscription_period.subscription
    cols = @subscription_period.dates.map do |date|
      {
          quantity: @subscription.quantity,
          amount_in_paisa: @subscription.vendor_product.price_in_paisa,
          amount_currency: @subscription.vendor_product.price_currency,
          date: date
      }
    end
    @subscription_period.daily_usages.import(cols)
  end
end
