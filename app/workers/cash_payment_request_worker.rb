class CashPaymentRequestWorker
  include Sidekiq::Worker

  def perform(cash_payment_id)
    cash_payment = CashPayment.find(cash_payment_id)
    return if cash_payment.nil?
    return if cash_payment.paid? || Rails.env == 'test'
    #TODO send mail to admin that user money has not been collected
    CashPaymentRequestWorker.perform_at(1.days.from_now, cash_payment.id)
  end
end
