require 'uri'
require 'net/http'

class Messaging

  def self.send(mobile_number, message)
    url = URI("http://api.msg91.com/api/sendhttp.php?country=91&sender=#{MilkyWay::MSG_HEADER}&route=4&mobiles=#{mobile_number}&authkey=#{Rails.application.credentials[:message][:key]}&message=#{message}")
    http = Net::HTTP.new(url.host, url.port)
    request = Net::HTTP::Get.new(url)
    http.request(request)
  end

end
