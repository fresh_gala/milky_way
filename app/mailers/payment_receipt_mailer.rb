class PaymentReceiptMailer < ApplicationMailer
  add_template_helper(ApplicationHelper)
  layout 'mailer_receipt'

  def send_receipt(payment_receipt)
    @payment_receipt = payment_receipt
    @user = @payment_receipt.user
    attachments["#{payment_receipt.receipt_number}.pdf"] = receipt_pdf
    mail to: "#{@user.name} <#{@user.email}>", subject: "You payment receipt for order ##{payment_receipt.receipt_number}"
  end

  private

  def receipt_pdf
    return nil if Rails.env == 'test'
    if @payment_receipt.image.attached?
      pdf = CombinePDF.new
      pdf << CombinePDF.parse(Net::HTTP.get(URI.parse(@payment_receipt.image.service_url)))
      pdf.to_pdf
    else
      @payment_receipt.generate_receipt_pdf
    end
  end

end
