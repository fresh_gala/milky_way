class TicketMailer < ApplicationMailer


  def welcome_alert(ticket)
    @ticket = ticket
    mail to: "#{ticket.name} <#{ticket.email}>", subject: 'Welcome to the Freshgala. Ticket registration successful.'
  end

end
