class VendorMailer < ActionMailer::Base
  default :from => 'support@freshgala.com'


  def vendor_add_email(user)
    @user = user
    mail(:to => @user.email,
         :subject => 'Vendor Registration')
  end
end