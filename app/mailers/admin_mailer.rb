class AdminMailer < ApplicationMailer

  def ticket_create_alert(ticket)
    @ticket = ticket
    mail to: "Admin <admin@freshgala.in>", subject: 'New Ticket has been raised.', from: 'Notification <notification@freshgala.in>'
  end
end
