class ApplicationMailer < ActionMailer::Base

  default from: 'Freshgala <support@freshgala.com>'
  layout 'mailer'

  before_action :set_logo


  private

  def set_logo
    attachments.inline['logo.png'] = File.read("#{Rails.root}/app/assets/images/logo.png")
  end

end
