json.wallet do
  json.created_at @wallet.created_at
  json.balance humanized_money_with_symbol(@wallet.balance)
  json.total_debits @wallet.transactions.debits.count
  json.total_credits @wallet.transactions.credits.count
end
