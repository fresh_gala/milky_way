json.transaction do
  json.(@transaction, :id, :date, :txn_type)
  json.amount humanized_money_with_symbol(@transaction.amount)
end
