json.items do
  json.array! @items do |item|
    json.(item, :id, :date, :quantity, :paused, :description, :delivered, :product_name, :subscription_id, :subscription_period_id)
    json.amount humanized_money_with_symbol(item.amount)
  end
end
