json.locations do
  json.array! @locations, :id, :latitude, :longitude, :address_line, :locality, :address
end
