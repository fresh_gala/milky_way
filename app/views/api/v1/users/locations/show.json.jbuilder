json.location do
  json.(@location, :id, :address_line, :locality, :latitude, :longitude, :geo_address)
end
