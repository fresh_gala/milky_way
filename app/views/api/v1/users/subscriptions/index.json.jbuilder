json.subscriptions do
  json.array! @subscriptions do |subscription|
    json.(subscription, :id, :start_on, :end_on, :active, :frequency, :quantity)
    json.product subscription.product.name
    json.vendor subscription.vendor.name
  end
end
