json.product do
  json.(@product, :id, :name, :description, :sample)
  json.images @product.images.map {|i| rails_blob_url(i)}
  json.amount humanized_money @product.base_price
  json.vendor_products @vendor_products do |vendor_product|
    json.(vendor_product, :id, :vendor_name, :min_quantity, :max_quantity)
    json.amount humanized_money vendor_product.price
  end
end
