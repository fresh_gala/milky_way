json.product_categories do
  json.array! @product_categories.each do |category, products|
    json.name category.name
    json.description category.description
    json.image rails_blob_url(category.image) if category.image.attached?
    json.products products do |product|
      json.(product, :id, :name, :description, :sample)
      json.amount humanized_money product.base_price
      json.images product.images.map {|i| rails_blob_url(i)}
    end
  end
end
