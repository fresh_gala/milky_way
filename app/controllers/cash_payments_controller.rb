class CashPaymentsController < BaseAdminController

  before_action :set_cash_payment, only: [:show, :update]

  def index
    @cash_payments = CashPayment.unpaid.join_associates(params[:text]).order(:created_at)
  end

  def all
    @cash_payments = CashPayment.all.join_associates(params[:text]).order(:created_at)
  end

  def show

  end

  def update
    if params[:cash_payment][:confirm] == 'Freshgala'
      @cash_payment.paid!
      redirect_to cash_payments_path
    else
      render js: "alert('Type Freshgala')"
    end
  end

  private

  def set_cash_payment
    @cash_payment = CashPayment.find params[:id]
  end
end
