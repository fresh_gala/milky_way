class ProductsController < BaseAdminController

  before_action :set_product, only: [:show, :edit, :update, :destroy]
  before_action :set_product_categories, only: [:new, :edit, :update]
  before_action :set_image, only: [:destroy]

  def index
    @products = Product.all.includes(:product_category)
  end

  def show

  end

  def new
    @product = Product.new
  end

  def edit

  end

  def create
    @product = Product.new(product_params)
    if @product.save
      flash[:success] = 'Product was successfully created!'
      redirect_to product_path(@product)
    else
      render js: "toastr['warning']('#{@product.errors.full_messages.to_sentence}')"
    end
  end

  def update
    if @product.update(product_params)
      flash[:success] = 'Product was successfully updated.!'
      redirect_to product_path(@product)
    else
      render js: "toastr['warning']('#{@product.errors.full_messages.to_sentence}')"
    end
  end

  def destroy
    @image.purge
    flash[:warning] = 'Image was successfully deleted'
    redirect_to product_path(@product)
  end

  private

  def set_product
    @product = Product.find(params[:id])
  end

  def set_product_categories
    @product_categories = ProductCategory.all
  end

  def product_params
    params.require(:product).permit(:name, :description, :unit_of_measurement, :base_price, :product_category_id, :freshgala_products, :sample, :images)
  end

  def set_image
    @image = @product.images.find(params[:image_id])
    return if @image.present?
    flash[:warning] = 'No image was found'
    redirect_to product_path(@product)
  end
end
