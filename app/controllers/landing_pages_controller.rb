class LandingPagesController < ApplicationController

  layout 'landing_page'

  def landing_page
    build_subscription_request
    @ticket = Ticket.new
  end

  def features

  end

  def about

  end

  def pricing
    
  end

  def create_ticket
    @ticket = Ticket.new(ticket_params)
    if @ticket.save
      flash[:success] = 'Your request has been placed. Our agent will contact you shortly!'
      redirect_to root_path
    else
      build_subscription_request
      flash[:danger] = 'Please check the errors.'
      render :landing_page
    end
  end

  def create_subscription_request
    @subscription_request = SubscriptionRequest.create(subscription_request_params)
  end

  private

  def build_subscription_request
    @subscription_request = SubscriptionRequest.new
  end

  def ticket_params
    params.require(:ticket).permit(:name, :email, :mobile, :locality).merge(status: 'waiting')
  end

  def subscription_request_params
    params.require(:subscription_request).permit(:name, :email, :mobile, :quantity, :product, :address)
  end
end
