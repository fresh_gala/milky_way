class Vendors::DeliveriesController < BaseVendorController

  before_action :set_date

  def index
    @deliveries = Vendor::DeliveryList.new(current_vendor, 'products', @date).all
  end

  private

  def set_date
    @date = if params[:date].present?
              Date.parse(params[:date])
            else
              Time.zone.now < Time.parse("10 am") ? Date.today : Date.tomorrow
            end
  end

end
