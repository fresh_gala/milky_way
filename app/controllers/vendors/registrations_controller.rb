# frozen_string_literal: true

class Vendors::RegistrationsController < ApplicationController

  def new
    @vendor = Vendor.new
  end

  def create
    @vendor = Vendor.new(vendor_params)
    if @vendor.save
      @vendor.send_otp!
    end
  end

  private

  def vendor_params
    params.require(:vendor).permit(:name, :mobile, :secondary_contact, :email)
  end

end
