class Vendors::LocationsController < BaseVendorController

  # skip_before_action :ensure_vendor_has_location, only: [:new]
  before_action :set_location, only: [:edit, :update, :index]

  def new
    @location = current_vendor.build_location
  end

  def index

  end

  def create
    @location = current_vendor.build_location(location_params)
    if @location.save
      redirect_to vendors_dashboards_path
    else
      render action: 'new'
    end
  end

  def edit

  end

  def update
    @location.update(location_params)
  end

  private

  def location_params
    params.require(:location).permit(:latitude, :longitude,
                                     :address_line_1, :address_line_2, :locality,
                                     :service_radius)
  end

  def set_location
    @location = current_vendor.location
  end

end
