class Vendors::UsersController < BaseVendorController

  before_action :set_group_by

  def index
    @vendor_list = Vendor::UserList.new(current_vendor, @group_by)
  end

  private

  def set_group_by
    @group_by = params[:group_by].present? ? params[:group_by] : 'users'
  end

end
