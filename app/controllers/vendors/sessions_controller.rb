# frozen_string_literal: true

class Vendors::SessionsController < Devise::SessionsController
  before_action :set_vendor, only: [:create, :resend_otp]
  include Devise::Controllers::Rememberable
  # before_action :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  # def new
  #   super
  # end

  # POST /resource/sign_in
  def create
    if @vendor.valid_otp?(params[:otp])
      unless @vendor.active?
        @vendor.update(active: true)
      end
      sign_in(:vendor, @vendor)
      remember_me(@vendor)
      redirect_to after_sign_in_path_for(:vendor)
    else
      render js: "toastr['error']('OTP is not valid.')"
    end
  end

  def resend_otp
    @vendor.send_otp!
  end

  def authenticate
    @vendor = Vendor.find_by(email: params[:account_identifier]) || Vendor.find_by(mobile: params[:account_identifier])
    @vendor.send_otp! if @vendor.present?
  end

  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end
  #

  private

  def set_vendor
    @vendor = Vendor.find_by(id: params[:id])
    return if @vendor.present?
    render js: "toastr['error']('This is not a valid request')"
  end
end
