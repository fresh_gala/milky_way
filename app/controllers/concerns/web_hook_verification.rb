module WebHookVerification


  private

  def verify_razor_pay_request
    begin
      Razorpay::Utility.verify_webhook_signature(request.body.read, request.headers['X-Razorpay-Signature'], Rails.application.credentials[:webhook][:razor_secret])
    rescue SecurityError
      render json: {errors: 'Invalid Request'}, status: :bad_request
    end
  end
end