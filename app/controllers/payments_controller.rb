class PaymentsController < BaseAdminController
  before_action :set_payment, only: [:show]

  def index
    @payments = Payment.all.includes(:user)
  end

  def show
    @payment_receipt = @payment.payment_receipt
  end

  private

  def set_payment
    @payment = Payment.find_by(id: params[:id])
  end

end
