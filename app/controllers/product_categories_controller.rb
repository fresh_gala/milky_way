class ProductCategoriesController < BaseAdminController


  before_action :set_product_category, only: [:edit, :update]

  def index
    @product_categories = ProductCategory.all.with_attached_image.order(:name)
  end

  def create
    @product_category = ProductCategory.new(product_category_params)
    if @product_category.save
      flash[:success] = 'Category Saved!'
      redirect_to product_categories_path
    else
      render js: "toastr['warning']('#{@product_category.errors.full_messages.to_sentence}')"
    end
  end

  def new
    @product_category = ProductCategory.new
  end

  def edit

  end

  def update
    if @product_category.update(product_category_params)
      flash[:success] = 'Category Saved!'
      redirect_to product_categories_path
    else
      render js: "toastr['warning']('#{@product_category.errors.full_messages.to_sentence}')"
    end
  end

  private

  def product_category_params
    params.require(:product_category).permit(:name, :image)
  end

  def set_product_category
    @product_category = ProductCategory.find_by id: params[:id]
  end
end
