class VendorsController < BaseAdminController
  before_action :set_vendor, only: [:edit, :update]

  def index
    @vendors = Vendor.all
  end

  def new
    @vendor = Vendor.new
  end

  def edit
  end

  def create
    @vendor = Vendor.new(vendor_params)
    if @vendor.save
      flash[:success] = 'Vendor was successfully created!'
      VendorMailer.vendor_add_email(@vendor).deliver
      redirect_to vendors_path
    else
      render js: "toastr['warning']('#{@vendor.errors.full_messages.to_sentence}')"
    end
  end

  def update
    if @vendor.update(vendor_params)
      flash[:success] = 'Vendor was successfully updated.!'
      redirect_to vendors_path
    else
      render js: "toastr['warning']('#{@vendor.errors.full_messages.to_sentence}')"
    end
  end

  private

  def set_vendor
    @vendor = Vendor.find_by id: params[:id]
  end

  def vendor_params
    params.require(:vendor).permit(:name, :email, :primary_contact, :secondary_contact, :password, :active)
  end
end
