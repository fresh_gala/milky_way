class TicketsController < BaseAdminController
  before_action :set_ticket, only: [:edit, :update]

  def index
    @tickets = Ticket.all
  end

  def subscription_request
    @subscriptions = SubscriptionRequest.all
  end

  def new
    @ticket = Ticket.new
  end

  def edit
  end

  def update
    if @ticket.update(ticket_params)
      flash[:success] = 'ticket was successfully updated.!'
      redirect_to tickets_path
    else
      redirect_to edit_ticket_path(@ticket)
      flash[:warning] = @ticket.errors.full_messages.join(', ')
    end
  end

  private

  def set_ticket
    @ticket = Ticket.find_by id: params[:id]
  end

  def ticket_params
    params.require(:ticket).permit(:status)
  end

end
