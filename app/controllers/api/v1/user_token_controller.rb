module Api::V1
  class UserTokenController < Knock::AuthTokenController
    include Knock::Authenticable
    skip_before_action :verify_authenticity_token

    def create
      if user.verified?
        render json: jwt_params, status: :ok
      else
        user.send_otp!
        render json: {
            errors: 'User not verified. Verify with the OTP sent to your mobile',
            user_id: user.id,
            mobile: user.protected_mobile
        }, status: :bad_request
      end
    end

    private

    def user
      entity
    end

    def authenticate
      if entity.present?
        return if entity.authenticate(auth_params[:password])
        render json: {errors: 'Password is not valid'}, status: :bad_request
      else
        render json: {errors: 'Email is not valid'}, status: :bad_request
      end
    end

    def jwt_params
      {
          jwt: auth_token.token,
          user: {
              id: user.id,
              mobile: user.id,
              email: user.email,
              name: user.name,
              image: user.image.attached? ? user.image.service_url : nil
          }
      }
    end
  end
end
