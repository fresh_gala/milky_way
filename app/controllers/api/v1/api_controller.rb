class Api::V1::ApiController < ActionController::API
  include Knock::Authenticable

  rescue_from(ActionController::ParameterMissing) do |parameter_missing_exception|
    error = "#{parameter_missing_exception.param.to_s.titleize} parameter is required"
    render json: {errors: error}, status: :unprocessable_entity
  end

end
