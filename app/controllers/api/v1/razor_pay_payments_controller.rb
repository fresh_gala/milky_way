module Api::V1
  class RazorPayPaymentsController < ApiController

    include WebHookVerification
    before_action :verify_razor_pay_request

    def create
      RazorPayment.new(params.permit!.to_h).create_txn
      render json: {message: 'Payment save successfully'}, status: :created
    end

  end
end
