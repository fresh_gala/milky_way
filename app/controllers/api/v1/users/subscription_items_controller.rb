module Api::V1::Users
  class SubscriptionItemsController < UserApiController

    before_action :set_date
    before_action :ensure_date_is_valid?, only: [:update]
    before_action :set_item, only: [:update]

    def index
      @items = DailyUsage.for_user(current_user.id, @date)
    end

    def update
      @item.update(update_params)
      #TODO send email to user if he pause
      #TODO solution to update quantity
      render json: {messages: 'Updated Successfully.', item: @item.attributes}, status: :ok
    end

    private

    def update_params
      params.require(:item).permit(:paused)
    end

    def set_date
      @date = params[:date].present? ? Date.parse(params[:date]) : Date.today
    end

    def ensure_date_is_valid?
      return if valid_date?
      render json: {errors: 'You cannot update this item.'}, status: :unprocessable_entity
    end

    def set_item
      @item = DailyUsage.for_user(current_user.id, @date).find_by(id: params[:id])
    end

    def valid_date?
      return false if @date < Date.today
      return Time.zone.now < DateTime.now.beginning_of_day + 20.hours if Date.today == @date
      true
    end

  end
end
