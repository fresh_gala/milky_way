module Api::V1::Users
  class TransactionsController < UserApiController

    before_action :set_transaction, only: [:show]

    def index
      @transactions = current_user.transactions.limit(50).offset(params[:page] || 0)
    end

    def show

    end

    private

    def transaction_params
      params.require(:transaction).permit(:amount, :status, :txn_source_id)
    end

    def set_transaction
      @transaction = current_user.transactions.find_by id: params[:id]
      return if @transaction.present?
      render json: {errors: 'Transaction cannot be found.'}, status: :unprocessable_entity
    end
  end
end
