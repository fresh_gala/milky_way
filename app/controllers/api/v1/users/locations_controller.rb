module Api::V1::Users
  class LocationsController < UserApiController

    before_action :set_location, only: [:show, :update]

    def index
      @locations = current_user.locations
    end

    def create
      location = current_user.locations.new(location_params)
      if location.save
        render json: {message: 'Location created successfully', location_id: location.id}, status: :created
      else
        render json: {errors: location.errors.full_messages.to_sentence}, status: :unprocessable_entity
      end
    end

    def show

    end

    def update
      if @location.update(location_params)
        render json: {message: 'Location updated successfully'}, status: :ok
      else
        render json: {errors: @location.errors.full_messages.to_sentence}, status: :unprocessable_entity
      end
    end

    private

    def location_params
      params.require(:location).permit(:latitude, :longitude, :address_line, :locality)
    end

    def set_location
      @location = current_user.locations.find_by(id: params[:id])
      return if @location.present?
      render json: {errors: 'Location cannot be found'}, status: :unprocessable_entity
    end
  end
end
