module Api::V1::Users
  class CashPaymentsController < UserApiController

    before_action :set_subscription

    def create
      if @parent.payment_entity.amount[:tokens].size.positive?
        obj = CashPayment.create(current_user, cash_payment_params[:time_slot], @parent)
        if obj.persisted?
          render json: {message: "Payment is successful"}, status: :created
        else
          render json: {errors: "Payment is not successful, #{obj.errors.full_messages.to_sentence}"}, status: :unprocessable_entity
        end
      end
    end

    private

    def cash_payment_params
      params.require(:cash_payment).permit(:id, :time_slot)
    end

    def set_subscription
      @parent = klass.find_by id: cash_payment_params[:id]
      return if @parent.present?
      render json: {errors: "#{klass.to_s} cannot be found"}, status: :unprocessable_entity
    end

    def klass
      params[:payment_to].present? ? params[:payment_to].constantize : Subscription
    end
  end
end
