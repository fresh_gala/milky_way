module Api::V1::Users
  class WalletsController < UserApiController

    def show
      @wallet = current_user.wallet
    end

  end
end
