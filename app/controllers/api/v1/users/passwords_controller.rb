module Api::V1::Users
  class PasswordsController < UserApiController
    skip_before_action :authenticate_user!
    before_action :search_user, only: [:create]
    before_action :set_user, only: [:update]

    #Todo send OTP to email also
    def create
      @user.send_otp!
      render json: {user: {name: @user.name, user_id: @user.id, mobile: @user.protected_mobile}, message: 'User is found'}, status: :ok
    end

    def update
      if @user.valid_otp?(params[:otp], update_otp: false)
        if @user.update(password: params[:password], password_confirmation: params[:password_confirmation])
          @user.invalidate_otp
          render json: {message: 'Password updated successfully and Login with your new password'}, status: :ok
        else
          render json: {errors: @user.errors.full_messages.to_sentence}, status: :unprocessable_entity
        end
      else
        render json: {errors: 'Otp is not valid'}, status: :unprocessable_entity
      end
    end

    private

    def search_user
      @user = User.verified_users.find_by(email: params[:account_identifier]) || User.verified_users.find_by(mobile: params[:account_identifier])
      return if @user.present?
      render json: {errors: 'User email or mobile not found'}, status: :unprocessable_entity
    end

    def set_user
      @user = User.verified_users.find_by(id: params[:id])
      return if @user.present?
      render json: {errors: 'User not found'}, status: :unprocessable_entity
    end
  end
end
