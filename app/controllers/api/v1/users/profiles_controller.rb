module Api::V1::Users
  class ProfilesController < UserApiController

    def update
      if current_user.update(user_params)
        render json: {message: 'User updated successfully', user_id: current_user.id}, status: :ok
      else
        render json: {errors: current_user.errors.full_messages.to_sentence}, status: :unprocessable_entity
      end
    end


    def show
      render json: {user: current_user.attributes.slice(*allowed_parameters)}, status: :ok
    end

    private

    def user_params
      params.require(:user).permit(:date_of_birth, :gender, :name)
    end

    def allowed_parameters
      %w(name email id date_of_birth gender created_at verified mobile)
    end
  end
end
