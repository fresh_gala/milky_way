module Api::V1::Users
  class SubscriptionsController < UserApiController

    before_action :set_subscription, only: [:destroy]

    def create
      @subscription = current_user.subscriptions.new(subscription_params)
      if @subscription.save
        render json: {subscription: create_response_params}, status: :created
      else
        render json: {errors: @subscription.errors.full_messages.join(', ')}, status: :unprocessable_entity
      end
    end

    def index
      @subscriptions = current_user.subscriptions.active.includes(vendor_product: :product)
    end

    def destroy
      if @subscription.draft
        @subscription.destroy
        render json: {message: 'Subscription is deleted'}, status: :ok
      else
        #TODO send error to admin
        render json: {errors: 'This subscription cannot deleted'}, status: :unprocessable_entity
      end
    end

    private

    def subscription_params
      params.require(:subscription).permit(:start_on, :frequency, :vendor_product_id, :quantity, :user_location_id)
    end

    def create_response_params
      {
          id: @subscription.id,
          name: 'Subscription'
      }.merge(@subscription.payment_entity.amount)
    end

    def set_subscription
      @subscription = current_user.subscriptions.find_by(id: params[:id])
    end

  end
end
