module Api::V1::Users
  class ProductsController < UserApiController

    before_action :set_location
    before_action :set_product, only: [:show]

    def index
      @product_categories = Product.near_by(@coordinates).with_attached_images.includes(:product_category).group_by(&:product_category)
    end

    def show
      if @coordinates.present?
        @vendor_products = @product.vendor_products.near_by_products(@coordinates)
      else
        render json: {errors: 'Location should be present'}, status: :unprocessable_entity
      end
    end

    private

    def set_location
      @coordinates = if params[:latitude].present? && params[:longitude].present?
                       [params[:latitude], params[:longitude]]
                     elsif params[:location_id].present?
                       current_user.locations.find(params[:location_id])&.coordinates
                     end
    end

    def set_product
      @product = Product.near_by(@coordinates).find_by id: params[:id]
      return if @product.present?
      render json: {errors: 'Product is not available'}, status: :unprocessable_entity
    end

  end
end
