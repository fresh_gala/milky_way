module Api::V1::Users
  class RegistrationsController < UserApiController
    skip_before_action :authenticate_user!
    before_action :set_user, except: [:create]

    def create
      user = User.new(user_params)
      if user.save
        user.send_otp!
        render json: {message: 'User created successfully', user_id: user.id}, status: :created
      else
        render json: {errors: user.errors.full_messages.to_sentence}, status: :unprocessable_entity
      end
    end

    def validate_user
      if @user.valid_otp?(params[:otp])
        @user.update(verified: true)
        render json: {
            message: 'Otp is valid.User registered successfully',
            jwt: generate_jwt_token,
            user: {
                id: @user.id,
                mobile: @user.id,
                email: @user.email,
                name: @user.name,
                image: @user.image.attached? ? rails_blob_url(@user.image) : nil
            }
        }, status: :ok
      else
        render json: {errors: 'Otp is not valid'}, status: :unprocessable_entity
      end
    end

    def resend_otp
      @user.send_otp!
      render json: {message: 'Otp is sent'}, status: :ok
    end

    private

    def user_params
      params.require(:user).permit(:name, :email, :mobile, :password, :password_confirmation)
    end

    def set_user
      @user = User.find_by(id: params[:id])
      return if @user&.not_verified?
      render json: {errors: 'User not found.'}, status: :unprocessable_entity
    end

    def generate_jwt_token
      Knock::AuthToken.new(payload: {sub: @user.id}).token
    end

  end
end
