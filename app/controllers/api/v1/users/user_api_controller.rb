module Api::V1
  class Users::UserApiController < ApiController
    before_action :authenticate_user!

    private

    def authenticate_user!
      authenticate_for User
      return if current_user&.verified?
      render json: {errors: 'User not found'}, status: :bad_request
    end
  end
end

