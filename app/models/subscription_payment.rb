class SubscriptionPayment

  def initialize(subscription, date = Date.today)
    @subscription = subscription
    @date = date
    @unpaid_sub_periods = subscription_periods
    @frequency = subscription.frequency
    update_wallet_to_periods
  end

  def amount
    {
        amount: (amount_to_be_paid - discount - wallet_balance).to_f,
        tokens: @unpaid_sub_periods.map(&:token),
        wallet: wallet_balance.to_f,
        discount: discount,
        total_amount: amount_to_be_paid.to_f
    }
  end

  private

  def amount_to_be_paid
    @unpaid_sub_periods.map(&:amount).sum
  end

  def discount
    0
  end

  def wallet_balance
    @subscription.user.wallet_balance
  end

  def subscription_periods
    if @subscription.draft?
      @subscription.subscription_periods.unpaid
    else
      @subscription.subscription_periods.unpaid.where(month: @date.beginning_of_month)
    end
  end

  def update_wallet_to_periods
    if @unpaid_sub_periods.exists? && wallet_balance > 0
      @unpaid_sub_periods.first.update(wallet: wallet_balance)
    end
  end

end
