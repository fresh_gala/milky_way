class SubscriptionRequest < ApplicationRecord
  STATUS = ['PENDING', 'CONVERTED', 'REJECTED']

  validates :name, presence: true
  validates :email, presence: true
  validates :mobile, presence: true, uniqueness: true
  validates :quantity, presence: true
  validates :address, presence: true
  validates :product, presence: true
  validates :status, inclusion: {in: STATUS, message: "%{value} is not a valid status"}

  def create_user_with_subcription
    user = User.create_and_send_notification(name: name, email: email, mobile_number: mobile)
    user_address = user.addresses.create(address_line1: address.address_line1, address_line2: address.address_line2, location: address.location)
    subscription = Subscription.create_with_daily_usages(address: user_address, product: product, quantity: quantity, start_on: 2.days.after, user: user)
    update(status: 'CONVERTED')
  end

  def reject_request
    update(status: 'REJECTED')
  end
end
