class Product < ApplicationRecord
  belongs_to :product_category
  has_many :vendor_products, inverse_of: :product

  has_many_attached :images

  validates :name, presence: true, uniqueness: true
  validate :one_image_attached?

  monetize :base_price_in_paisa, as: 'base_price'

  scope :near_by, -> (coordinates) do
    joins("inner join (#{VendorProduct.near_by_products(coordinates).unscope(:select).select('vendor_products.product_id').group('1').to_sql}) vendor_products on vendor_products.product_id = products.id")
        .select('products.*')
  end
  scope :with_pre_loaded_images, -> {preload(images_attachments: :blob)}

  private

  def one_image_attached?
    return if images.size > 0
    errors.add(:images, 'should be present') unless images.attached?
  end
end
