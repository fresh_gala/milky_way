class VendorLocation < ApplicationRecord

  belongs_to :vendor, inverse_of: :location
  has_many :payment_receipts, inverse_of: :vendor_location

  validates_presence_of :address_line_1, :locality, :longitude, :latitude

  geocoded_by :address

  def geo_address
    Geocoder.address([latitude, longitude])
  end

  def address_array
    arr = []
    arr << address_line_1
    arr << address_line_2 if address_line_2.present?
    arr << locality
    arr << geo_address
    arr
  end

  def address
    address_array.join(',')
  end

end
