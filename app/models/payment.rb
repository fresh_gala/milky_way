class Payment < ApplicationRecord

  has_paper_trail

  belongs_to :user, inverse_of: :payments
  has_one :user_transaction, inverse_of: :payment
  has_many :payment_tokens, inverse_of: :payment
  has_one :cash_payment, inverse_of: :payment
  has_one :payment_receipt, inverse_of: :payment

  monetize :amount_in_paisa, as: 'amount'
  monetize :wallet_in_paisa, as: 'wallet'
  monetize :total_in_paisa, as: 'total'
  monetize :discount_in_paisa, as: 'discount'

  validates_presence_of :amount, :paid_through, :user_id, :status
  validates :source_id, uniqueness: {scope: :paid_through}, if: Proc.new {|a| a.paid_through != 'Cash'}

  def user_location
    payment_tokens.first.parent.user_location
  end

  def vendor
    payment_tokens.first.parent.vendor
  end

  def vendor_location
    payment_tokens.first.parent.vendor_location
  end

  def create_receipt
    return if payment_receipt.present?
    PaymentReceiptWorker.perform_async(self.id)
  end

end
