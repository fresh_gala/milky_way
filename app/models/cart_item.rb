class CartItem < ApplicationRecord

  belongs_to :cart, inverse_of: :cart_items
  belongs_to :vendor_product
  belongs_to :product
  has_one :user, through: :cart

  monetize :price_in_paisa, as: 'price'

end
