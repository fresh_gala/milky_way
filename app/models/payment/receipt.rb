class Payment::Receipt

  def initialize(payment)
    @payment = payment
  end

  def create
    PaymentReceipt.transaction do
      create_receipt
      create_items
    end
  end

  private

  def create_receipt
    @payment_receipt = @payment.create_payment_receipt(
        user: @payment.user,
        vendor: @payment.vendor,
        date: @payment.date,
        status: 'Paid',
        user_location_id: @payment.user_location.id,
        vendor_location_id: @payment.vendor_location.id,
        wallet: @payment.wallet,
        discount: @payment.discount,
        user_location_text: @payment.user_location.address_array,
        vendor_location_text: @payment.vendor_location.address_array,
        paid_through: @payment.paid_through
    )
  end

  def create_items
    @payment.payment_tokens.each do |payment_token|
      @payment_receipt.payment_receipt_items.create(
          parent_type: payment_token.parent_type,
          parent_id: payment_token.parent_id,
          quantity: payment_token.parent.dates.count,
          unit_of_measurement: payment_token.parent.unit_of_measurement,
          rate: payment_token.parent.product_price,
          amount: payment_token.parent.amount,
          description: payment_token.parent.description
      )
    end
  end

end
