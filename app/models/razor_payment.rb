class RazorPayment

  attr_reader :errors, :user, :txn

  def initialize(attrs = {})
    @errors = []
    @attrs = attrs.deep_symbolize_keys
    set_user
  end

  def create_txn
    @payment = Payment.create(
        source_id: get_value('id'),
        amount: Money.new(get_value('amount')),
        status: get_value('status'),
        user: @user,
        source_document: @attrs,
        paid_through: 'RazorPay',
        date: Time.zone.now
    )
    if @payment.errors.present?
      @errors += @payment.errors.full_messages
      #TODO send error message to admin
    else
      @tokens = get_value(value = 'tokens')
      update_tokens
      @payment.create_receipt
    end
  end

  private

  def set_user
    @user = User.find_by(email: get_value)
    return if @user.present?
    @errors << 'User cannot be found'
  end

  def get_value(value = 'email')
    @attrs[:payload][:payment][:entity][value.to_sym]
  end

  def update_tokens
    tokens = @user.payment_tokens.unpaid.where(token: @tokens)
    if tokens.exists?
      tokens.each do |token|
        token.paid!(@payment)
      end
    end
    discount = tokens.map(&:parent).sum(&:discount_amount)
    wallet = tokens.map(&:parent).sum(&:wallet)
    @payment.update(
        wallet: wallet,
        discount: discount,
        total: @payment.amount + wallet + discount
    )
  end

end
