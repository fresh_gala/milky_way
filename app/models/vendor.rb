class Vendor < ApplicationRecord
  devise :registerable, :rememberable, :database_authenticatable

  has_many :vendor_products, inverse_of: :vendor
  has_many :products, through: :vendor_products
  has_many :subscriptions, through: :vendor_products
  has_many :users, through: :subscriptions
  has_one :location, class_name: 'VendorLocation', inverse_of: :vendor
  has_many :payment_receipts, inverse_of: :vendor
  before_create :set_password

  validates :name, presence: true
  validates :email, presence: true, length: {in: 6..150},
            uniqueness: {case_sensitive: false, message: "is already registered with us"},
            format: {with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i}
  validates :mobile, presence: true, length: {is: 10}, uniqueness: {case_sensitive: false, message: "is already registered with us"}

  include OtpAuthentication

  def active_users
    users.where(subscriptions: {active: true}, users: {verified: true}).uniq
  end

  private

  def set_password
    ps = SecureRandom.hex(10)
    self.password = ps
    self.password_confirmation = ps
  end

end
