class VendorProduct < ApplicationRecord

  belongs_to :vendor, inverse_of: :vendor_products
  belongs_to :product, inverse_of: :vendor_products
  has_one :product_category, through: :product
  has_many :subscriptions, inverse_of: :vendor_product

  monetize :price_in_paisa, as: 'price'

  scope :near_by_products, -> (coordinates) do
    select_string = [
        'vendors.name as vendor_name',
    ]
    base_scope = joins(:vendor)
    if coordinates.present?
      base_scope = base_scope
                       .joins("inner join (#{VendorLocation.near(coordinates, :service_radius, units: :km).to_sql}) locations on locations.vendor_id = vendors.id")
    end
    base_scope.select("vendor_products.*, #{select_string.join(', ')}")
  end

end
