class Cart < ApplicationRecord
  belongs_to :user, inverse_of: :cart
  has_many :cart_items, inverse_of: :cart
end
