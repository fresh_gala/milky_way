class ProductCategory < ApplicationRecord
  has_many :products
  has_one_attached :image

  validates :name, presence: true, uniqueness: true
  validate :image_attached?

  private

  def image_attached?
    errors.add(:image, 'should be present') unless image.attached?
  end
end
