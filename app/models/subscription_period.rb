class SubscriptionPeriod < ApplicationRecord

  belongs_to :subscription, inverse_of: :subscription_periods, optional: false
  has_one :vendor, through: :subscription
  has_one :user, through: :subscription
  has_one :product, through: :subscription
  has_one :vendor_product, through: :subscription
  has_many :daily_usages, inverse_of: :subscription_period, dependent: :delete_all
  has_one :payment_token, foreign_key: 'parent_id'

  before_create :set_amount

  after_create_commit do
    PaymentToken.create(parent: self, user: user, token: "SUB#{Time.zone.now.strftime("%Y%m%d%H%M%S%L")}")
    create_daily_usages
  end

  before_validation :validate_month_for_subscription, on: :create
  validates :month, presence: true

  monetize :amount_in_paisa, as: 'amount'
  monetize :wallet_in_paisa, as: 'wallet'
  monetize :discount_amount_in_paisa, as: 'discount_amount'

  scope :paid, -> {where(paid: true)}
  scope :unpaid, -> {where(paid: false)}

  def token
    payment_token.token
  end

  def paid!
    update(paid: true, paid_at: Time.zone.now)
    user.wallet.debit(wallet) if wallet > 0
    #TODO send payment confirmation email
    #TODO if this is first time payment send email to user and also admin
    if subscription.first_time_payment?
      subscription.activate!
    end
  end

  def dates
    return [] if month.nil?
    subscription.frequency_entity.dates(month)
  end

  def product_price
    subscription.vendor_product.price
  end

  def create_daily_usages
    DailyUsageWorker.perform_async(self.id)
  end

  def user_location
    subscription.user_location
  end

  def vendor_location
    subscription.vendor.location
  end

  def description
    "Subscription of #{product.name} for #{month.strftime '%b %Y'}"
  end

  def unit_of_measurement
    'days'
  end

  private

  def set_amount
    return if month.nil?
    self.amount = (dates.count * product_price)
  end

  def validate_month_for_subscription
    months = subscription.subscription_periods.pluck(:month).map {|d| d.strftime('%B %Y')}
    return if self.month.nil? || !self.month.strftime('%B %Y').in?(months)
    errors.add(:base, 'already created for this month')
  end

end
