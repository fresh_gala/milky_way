class Vendor::UserList

  def initialize(vendor, group_by)
    @vendor, @group_by = vendor, group_by
  end

  def list
    send("group_by_#{@group_by}")
  end

  private

  def group_by_users
    User.with_json_subscriptions(@vendor)
  end

  def group_by_products
    #TODO group by products
  end

end