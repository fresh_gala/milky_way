class Vendor::DeliveryList
  def initialize(vendor, group_by, date = Date.today)
    @vendor = vendor
    @group_by = group_by || 'products'
    @date = date
  end

  def all
    send("group_by_#{@group_by}")
  end

  private

  def group_by_products
    DailyUsage.vendor_list(@vendor, @date).group_by(&:product_name)
  end

  def group_by_users
    #TODO
  end

end