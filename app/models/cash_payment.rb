class CashPayment < ApplicationRecord
  DUE_DAYS = 2

  belongs_to :user, inverse_of: :cash_payments
  belongs_to :payment, inverse_of: :cash_payment
  has_many :payment_tokens, through: :payment

  monetize :amount_in_paisa, as: 'amount'

  scope :unpaid, -> {where(paid: false)}

  scope :join_associates, ->(query = nil) do
    base_query = joins(:user, :payment)
                     .select("cash_payments.*, users.name as user_name")
    if query.present?
      base_query = base_query.where('lower(users.name) LIKE :text OR lower(users.email) LIKE :text OR lower(users.mobile) LIKE :text', text: "%#{query.downcase}%")
    end
    base_query
  end

  before_validation :set_due_date
  validates_presence_of :date, :due_date, :time_slot
  after_create_commit do
    CashPaymentRequestWorker.perform_at(2.days.from_now, self.id)
  end

  def paid!
    update(paid: true, paid_at: Time.zone.now)
    payment.update(status: 'Paid')
    payment.create_receipt
  end

  def self.create(user, time_slot, parent)
    amount_hash = parent.payment_entity.amount
    obj = CashPayment.transaction do
      payment = Payment.create(
          amount: amount_hash[:amount],
          total: amount_hash[:total],
          discount: amount_hash[:discount],
          wallet: amount_hash[:wallet],
          status: 'Payment To be Collected',
          user: user,
          paid_through: 'Cash',
          date: Time.zone.now
      )
      return payment if payment.errors.present?
      cash_payment = user.cash_payments.create(payment: payment, date: Date.today, time_slot: time_slot, amount: payment.amount)
      if cash_payment.errors.present?
        payment.delete
      end
      return cash_payment if cash_payment.errors.present?
      user.payment_tokens.where(token: amount_hash[:tokens]).each do |token|
        token.paid!(payment, 'Cash')
      end
      cash_payment
    end
    obj
  end

  private

  def set_due_date
    self.due_date = Date.today + DUE_DAYS.days
  end

end
