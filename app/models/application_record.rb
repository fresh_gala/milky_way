class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  scope :by_created, -> {order(created_at: :asc)}
  scope :earliest_created, -> {by_created.first}
  scope :most_recently_created, -> {by_created.last}
end
