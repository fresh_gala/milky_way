class PaymentReceiptItem < ApplicationRecord

  belongs_to :payment_receipt, inverse_of: :payment_receipt_items

  monetize :rate_in_paisa, as: 'rate'
  monetize :amount_in_paisa, as: 'amount'

end
