class Subscription < ApplicationRecord

  FREQUENCIES = ['Every Day', 'Every 3 Days', 'Every 7 Days', 'Alternate Day']
  DATE_AFTER_PAY_FOR_TWO_MONTHS = Date.today.end_of_month - 4.days

  belongs_to :user, inverse_of: :subscriptions, optional: false
  belongs_to :vendor_product, inverse_of: :subscriptions, optional: false
  belongs_to :user_location, inverse_of: :subscriptions, optional: false
  has_one :product, through: :vendor_product
  has_one :vendor, through: :vendor_product
  has_many :subscription_periods, inverse_of: :subscription, dependent: :destroy

  validates :quantity, :start_on, :frequency, presence: true
  validates_inclusion_of :frequency, in: FREQUENCIES
  before_validation :ensure_start_on_is_valid?
  before_destroy :ensure_this_can_be_destroyed?
  before_validation :check_product_is_within_user_location?, on: :create

  scope :active, -> {where(draft: false, active: true)}

  scope :with_parents, -> do
    joins(vendor_product: :product)
        .select('subscriptions.*, products.name as product_name')
  end

  after_create_commit :create_subscription_period

  def frequency_entity
    SubscriptionFrequency.new self
  end

  def payment_entity
    SubscriptionPayment.new self
  end

  def create_period(month)
    subscription_periods.create(month: month.beginning_of_month)
  end

  def activate!
    update(draft: false, active: true)
  end

  def first_time_payment?
    subscription_periods.paid.size == 1
  end

  private

  def ensure_start_on_is_valid?
    return if start_on.present? && start_on > Date.today
    errors.add(:start_on, 'should be greater that today\'s date')
  end

  def create_subscription_period
    if start_on.strftime('%b %Y') == Date.today.strftime('%b %Y')
      if start_on < DATE_AFTER_PAY_FOR_TWO_MONTHS
        create_period(start_on)
      else
        create_period(start_on)
        create_period(start_on.next_month)
      end
    else
      create_period(start_on)
    end
  end

  def ensure_this_can_be_destroyed?
    return unless draft?
    errors.add(:base, 'this cannot be destroyed')
  end

  def check_product_is_within_user_location?
    return if vendor_product.nil? || user_location_id.nil?
    return if VendorProduct.near_by_products(user_location.coordinates).include?(vendor_product)
    errors.add(:base, 'Product cannot be delivered to your location. Kindly choose another product.')
  end

end
