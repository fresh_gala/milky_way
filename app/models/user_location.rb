class UserLocation < ApplicationRecord

  belongs_to :user, inverse_of: :locations
  has_many :subscriptions, inverse_of: :user_location
  has_many :payment_receipts, inverse_of: :user_location

  validates_presence_of :address_line, :locality, :longitude, :latitude

  def geo_address
    Geocoder.address([latitude, longitude])
  end

  def coordinates
    [latitude, longitude]
  end

  def address_array
    arr = []
    arr << address_line
    arr << locality
    arr << geo_address
    arr
  end

  def address
    address_array.join(',')
  end
end
