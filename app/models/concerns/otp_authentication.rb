module OtpAuthentication
  extend ActiveSupport::Concern

  def send_otp!
    otp = generate_otp
    self.update(encrypted_otp: BCrypt::Password.create(otp), opt_expires_in: 10.minutes.from_now)
    SendOtpWorker.perform_async(id, otp, self.class.to_s)
  end

  def generate_otp
    SecureRandom.random_number(100000..999999)
  end

  def valid_otp?(otp, update_otp: true)
    return false unless (otp.to_i == BCrypt::Password.new(encrypted_otp) && opt_expires_in > Time.zone.now)
    invalidate_otp if update_otp
    true
  end

  def invalidate_otp
    update(encrypted_otp: BCrypt::Password.create('xxxxxx'))
  end
end
