class Wallet < ApplicationRecord

  has_paper_trail

  belongs_to :user, inverse_of: :wallet
  has_many :transactions, class_name: 'UserTransaction', inverse_of: :wallet

  monetize :balance_in_paisa, as: 'balance'

  def txn_balance
    transactions.credits.sum(&:amount) - transactions.debits.sum(&:amount)
  end

  def credit(amount)
    transactions.create(
        amount: amount,
        user: user,
        txn_type: 'Credit',
        date: Time.zone.now
    )
    self.update(balance: balance + amount)
  end

  def debit(amount)
    transactions.create(
        amount: amount,
        user: user,
        txn_type: 'Debit',
        date: Time.zone.now
    )
    self.update(balance: balance - amount)
  end

end
