class Ticket < ApplicationRecord
  VALID_STATUS = ['waiting', 'process', 'active', 'inactive']

  validates :name, presence: true
  validates :email, presence: true, format: {with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i}
  validates :mobile, presence: true, numericality: true
  validates :locality, presence: true
  validates :status, inclusion: {in: VALID_STATUS, message: "%{value} is not a valid status"}

  after_create :send_welocome_email

  def send_welocome_email
    TicketAlertWorker.perform_async(self.id)
  end

  def status_enum
    VALID_STATUS
  end
end
