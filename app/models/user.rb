class User < ApplicationRecord
  has_secure_password

  has_many :access_grants, class_name: "Doorkeeper::AccessGrant",
           foreign_key: :resource_owner_id,
           dependent: :delete_all # or :destroy if you need callbacks

  has_many :access_tokens, class_name: "Doorkeeper::AccessToken",
           foreign_key: :resource_owner_id,
           dependent: :delete_all # or :destroy if you need callbacks
  has_many :locations, class_name: 'UserLocation', inverse_of: :user
  has_one :wallet, inverse_of: :user
  has_many :transactions, class_name: 'UserTransaction', inverse_of: :user
  has_many :payments, inverse_of: :user
  has_one :cart, inverse_of: :user
  has_many :cart_items, through: :cart
  has_many :subscriptions, inverse_of: :user
  has_many :subscription_periods, through: :subscriptions
  has_many :daily_usages, through: :subscription_periods
  has_many :payment_tokens, inverse_of: :user
  has_many :cash_payments, inverse_of: :user
  has_many :payment_receipts, inverse_of: :user

  has_one_attached :image

  include OtpAuthentication

  after_create_commit :create_wallet!
  after_create_commit :create_cart!

  validates_presence_of :name
  validates :email, presence: true, length: {in: 6..150},
            uniqueness: {case_sensitive: false, message: "is already registered with us"},
            format: {with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i}
  validates :mobile, presence: true, length: {is: 10}, uniqueness: {case_sensitive: false,
                                                                    message: "is already registered with us"}

  scope :verified_users, -> {where(verified: true)}
  scope :not_verified_users, -> {where(verified: false)}

  scope :with_json_subscriptions, ->(vendor) do
    User
        .joins("inner join (#{vendor.subscriptions.active.with_parents.to_sql}) as subscriptions on subscriptions.user_id = users.id")
        .select("users.*, jsonb_agg(to_jsonb(subscriptions)) AS subscription_list")
        .group('users.id')
  end

  def not_verified?
    !verified?
  end

  def wallet_balance
    wallet.balance
  end

  def protected_mobile
    "#{mobile.first(2)}xxx#{mobile[5..6]}xx#{mobile.last}"
  end

end
