class PaymentToken < ApplicationRecord

  belongs_to :user, inverse_of: :payment_tokens
  belongs_to :parent, polymorphic: true
  belongs_to :payment, inverse_of: :payment_tokens, optional: true
  has_one :cash_payment, inverse_of: :payment_token

  scope :unpaid, -> {where(paid: false)}

  def paid!(payment, type = 'RazorPay')
    update(paid: true, paid_at: Time.zone.now, payment: payment, payment_type: type)
    parent.paid!
  end

end
