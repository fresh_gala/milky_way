class DailyUsage < ApplicationRecord

  belongs_to :subscription_period, inverse_of: :daily_usages, optional: false
  has_one :user, through: :subscription_period
  has_one :product, through: :subscription_period
  has_one :vendor_product, through: :subscription_period

  validates :date, presence: true, uniqueness: {scope: :subscription_period}
  validates :quantity, presence: true

  monetize :amount_in_paisa, as: 'amount'

  scope :for_user, -> (user_id, date) do
    cols = [
        'products.name as product_name',
        'subscriptions.id as subscription_id',
        'subscription_periods.id as subscription_period_id',
    ]
    joins(:user, :product)
        .where('subscriptions.draft = false AND subscriptions.active = true')
        .where('users.id = ? AND daily_usages.date = ?', user_id, date)
        .select("daily_usages.*, #{cols.join(',')}")
  end

  scope :vendor_list, ->(vendor, date) do
    cols = [
        'products.name as product_name',
        'products.id as product_id',
        'users.name as user_name',
        'users.email as user_email',
        'users.mobile as user_mobile',
    ]
    joins(:user, vendor_product: [:vendor, :product])
        .where(vendors: {id: vendor.id}, daily_usages: {date: date})
        .select("daily_usages.*, #{cols.join(',')}")
  end

end
