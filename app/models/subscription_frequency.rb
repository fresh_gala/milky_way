class SubscriptionFrequency

  def initialize(subscription)
    @subscription = subscription
  end

  def dates(month = Date.today.next_month)
    @month = @subscription.end_on.present? ? @subscription.end_on : month.end_of_month
    get_dates
  end

  private

  def get_dates
    return [] if @subscription.frequency.nil?
    send("get_#{@subscription.frequency.parameterize(separator: '_')}")
  end

  def get_every_day
    (@subscription.start_on..@month).to_a.select {|d| d.year == @month.year && d.month == @month.month}
  end

  def get_every_3_days
    @subscription.start_on.step(@month, 3).select {|d| d.year == @month.year && d.month == @month.month}
  end

  def get_every_7_days
    @subscription.start_on.step(@month, 7).select {|d| d.year == @month.year && d.month == @month.month}
  end

  def get_alternate_day
    (@subscription.start_on - 1.day).step(@month, 1).select {|d| d.year == @month.year && d.month == @month.month}
  end

end
