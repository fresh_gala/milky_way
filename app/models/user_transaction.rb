class UserTransaction < ApplicationRecord

  has_paper_trail

  belongs_to :user, inverse_of: :transactions
  belongs_to :wallet, inverse_of: :transactions
  belongs_to :payment, inverse_of: :user_transaction, optional: true

  monetize :amount_in_paisa, as: 'amount'

  before_destroy {raise ReadOnlyRecord}

  scope :credits, -> do
    where(txn_type: 'Credit')
  end

  scope :debits, -> do
    where(txn_type: 'Debit')
  end

  def readonly?
    new_record? ? false : true
  end

end
