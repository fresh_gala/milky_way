class PaymentReceipt < ApplicationRecord

  belongs_to :user, inverse_of: :payment_receipts
  belongs_to :vendor, inverse_of: :payment_receipts
  belongs_to :user_location, inverse_of: :payment_receipts
  belongs_to :vendor_location, inverse_of: :payment_receipts
  belongs_to :payment, inverse_of: :payment_receipt
  has_many :payment_receipt_items, inverse_of: :payment_receipt

  after_create_commit do
    create_image
    send_email_to_user
  end

  before_validation :set_receipt_number, on: :create

  has_one_attached :image

  monetize :wallet_in_paisa, as: 'wallet'
  monetize :discount_in_paisa, as: 'discount'

  validates_presence_of :date, :paid_through
  validates :receipt_number, presence: true, length: {maximum: 20, minimum: 20}, uniqueness: true

  def total
    payment_receipt_items.sum(&:amount)
  end

  def grand_total
    total - wallet - discount
  end

  def generate_receipt_pdf
    WickedPdf.new.pdf_from_string(
        ApplicationController.new.render_to_string(partial: 'payments/receipt_layout',
                                                   layout: 'layouts/receipt_pdf_layout.html',
                                                   assigns: {payment_receipt: self}),
        pdf: "#{receipt_number}",
        margin: {top: 15}
    )
  end

  def send_email_to_user
    SendPaymentReceiptWorker.perform_async(self.id)
  end

  private

  def set_receipt_number
    self.receipt_number = "FGR#{Time.zone.now.strftime("%Y%m%d%H%M%S%L")}"
  end

  def create_image
    image.attach(
        io: StringIO.new(generate_receipt_pdf),
        filename: "#{receipt_number}.pdf",
        content_type: "application/pdf")
  end

end
