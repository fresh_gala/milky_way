require 'sidekiq/web'

Rails.application.routes.draw do

  root 'landing_pages#landing_page'

  devise_for :admins, only: [:sessions]
  devise_for :vendors, only: [:sessions, :registrations], controllers: {
      sessions: "vendors/sessions",
      registrations: "vendors/registrations"
  }
  devise_scope :vendor do
    get 'vendors/sessions/resend_otp', as: 'vendor_resend_otp'
    post 'vendors/sessions/authenticate', as: 'vendor_authentication'
  end

  if Rails.env.development?
    mount Sidekiq::Web => '/sidekiq'
  else
    authenticate :admin do
      mount Sidekiq::Web => '/sidekiq'
    end
  end

  authenticated :vendor do
    root 'vendors/dashboards#index'
    namespace 'vendors' do
      resources :dashboards, only: [:index]
      resources :locations, only: [:index, :new, :create, :edit, :update]
      resources :users, only: [:index]
      resources :deliveries, only: [:index]
    end
  end

  authenticated :admin do
    root 'dashboards#index'
    resources :dashboards, only: [:index]
    resources :product_categories, except: [:show, :destroy]
    resources :tickets, except: [:destroy, :new, :show]
    get 'subscription_request' => 'tickets#subscription_request'
    resources :products
    get 'cash_payments/all', to: 'cash_payments#all', as: 'all_cash_payments'
    resources :cash_payments, only: [:index, :update, :show]
    resources :payments, only: [:index, :show]
  end

  post 'user_token', to: 'api/v1/user_token#create', as: 'user_token_create'
  scope module: 'api' do
    namespace :v1 do
      namespace :users do
        resources :registrations, only: [:create]
        resource :profiles, only: [:update, :show]
        post 'registrations/validate_user', as: 'validate_user'
        get 'resend_otp', to: 'registrations#resend_otp', as: 'resend_otp'
        resources :locations, only: [:index, :create, :show, :update], defaults: {format: :json}
        resource :wallets, only: [:show], defaults: {format: :json}
        resources :transactions, only: [:show, :index], defaults: {format: :json}
        resources :products, only: [:index, :show], defaults: {format: :json}
        resources :subscription_items, only: [:index, :update], defaults: {format: :json}
        resources :subscriptions, only: [:create, :index, :destroy], defaults: {format: :json}
        resources :cash_payments, only: [:create]
        resources :passwords, only: [:create]
        resource :passwords, only: [:update]
      end
      resources :razor_pay_payments, only: [:create]
    end
  end

  get 'landing_page' => 'landing_pages#landing_page'
  get 'features' => 'landing_pages#features'
  get 'about' => 'landing_pages#about'
  get 'pricing' => 'landing_pages#pricing'
  post 'create_ticket' => 'landing_pages#create_ticket'
  get 'subscription_request' => 'landing_pages#new_subscription_request'
  post 'subscription_request' => 'landing_pages#create_subscription_request'

end
