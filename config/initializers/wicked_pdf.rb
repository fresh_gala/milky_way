WickedPdf.config = {
    :margin => {:top => 0},
    :header => {
        :margin => {:top => 0, :bottom => 0}, # margin on header
    },
    :footer => {
        :margin => {:top => 0, :bottom => 0}, # margin on footer
    }
}
