require 'test_helper'

class DashboardsControllerTest < ActionDispatch::IntegrationTest
  test "get index" do
    sign_in admin
    get dashboards_path
    assert_response :success
  end

  private

  def admin
    admins(:fg_admin)
  end
end
