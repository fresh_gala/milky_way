require 'test_helper'

class ProductCategoriesControllerTest < ActionDispatch::IntegrationTest

  test "get index" do
    sign_in admin
    get product_categories_path
    assert_response :success
  end

  test 'get new' do
    sign_in admin
    get new_product_category_path
    assert_response :success
  end

  test 'get edit' do
    sign_in admin
    get edit_product_category_path(product_category)
    assert_response :success
  end

  test 'create new and update' do
    sign_in admin
    assert_difference 'ProductCategory.count' do
      post product_categories_path, params: {
          product_category: {
              name: 'TONED MILK',
              image: fixture_file_upload('files/product_category.jpg')
          }
      }
    end

    product_category_new = ProductCategory.most_recently_created

    assert_equal 'TONED MILK', product_category_new.name
    patch product_category_path(product_category_new), params: {
        product_category: {
            name: 'Toned Milk'
        }
    }
    assert_equal 'Toned Milk', product_category_new.reload.name
  end

  private

  def admin
    admins(:fg_admin).reload
  end

  def product_category
    product_categories(:freshgala_milk).reload
  end

end
