require 'test_helper'

class Api::V1::RazorPayPaymentsControllerTest < ActionDispatch::IntegrationTest
  test "create from them" do
    travel_to Date.today.beginning_of_month + 5.days do
      jay.wallet.update(balance: 600)
      assert sub_two.draft?
      refute sub_two.active?
      refute payment_token.paid?
      Sidekiq::Testing.inline! do
        assert_difference [-> {jay.payments.count}, -> {jay.payment_receipts.count}] do
          post v1_razor_pay_payments_path, headers: p_headers, params: payment_params.to_json
          assert_response :success
        end
      end
      refute sub_two.draft?
      assert payment_token.paid?
      assert sub_two.active?
      assert_equal 0, jay.reload.wallet_balance.to_i
    end
  end

  private

  def jay
    users(:jay)
  end

  def p_headers
    {
        'X-Razorpay-Signature': OpenSSL::HMAC.hexdigest('SHA256', Rails.application.credentials[:webhook][:razor_secret], payment_params.to_json),
        'Content-Type': 'application/json'
    }
  end

  def payment_params
    {
        event: "payment.authorized",
        entity: "event",
        contains: ["payment"],
        payload: {
            payment: {
                entity: {
                    id: "pay_6X6jcHoHdRdy79",
                    entity: "payment",
                    amount: payment[:amount].to_f,
                    tokens: payment[:tokens],
                    currency: "INR",
                    status: "authorized",
                    amount_refunded: 0,
                    refund_status: '',
                    method: "card",
                    order_id: "order_6X4mcHoSXRdy79",
                    card_id: "card_6GfX4mcIAdsfDQ",
                    bank: '',
                    captured: true,
                    email: "jay@modernfamily.com",
                    contact: "9367733300",
                    description: "Payment Description",
                    error_code: '',
                    error_description: '',
                    fee: 200,
                    service_tax: 10,
                    international: false,
                    notes: {
                        reference_no: "848493"
                    },
                    wallet_amount: 60000,
                    vpa: '',
                    wallet: ''
                }
            },
            created_at: 1400826760
        }
    }.deep_stringify_keys
  end

  def payment
    sub_two.payment_entity.amount
  end

  def sub_two
    subscriptions(:jay_sub_two).reload
  end

  def payment_token
    payment_tokens(:jay_sub_two_token_one).reload
  end
end
