require 'test_helper'

class Api::V1::Users::SubscriptionsControllerTest < ActionDispatch::IntegrationTest

  test "create subscription" do
    assert_difference 'user.subscriptions.count' do
      post v1_users_subscriptions_path, headers: authenticated_header(user), params: {
          subscription: valid_params
      }
    end
    res =  parsed_response(response)['subscription']
    subscription = Subscription.find(res['id'])
    month_days_count = subscription.frequency_entity.dates(subscription.subscription_periods.first.month).count
    assert_equal month_days_count, (Date.today.next_month.beginning_of_month..Date.today.next_month.end_of_month).to_a.count
    amount = (subscription.frequency_entity.dates(Date.today.next_month.beginning_of_month).count * subscription.vendor_product.price).to_f
    assert_equal 1, res['tokens'].count
    assert_equal amount, res['amount']
    assert_equal amount, res['total_amount']
    assert_equal 0, res['discount']
    assert_equal 0.0, res['wallet']
  end

  test 'create from mid of month' do
    Timecop.travel Date.today.beginning_of_month + 15.days do
      assert_difference 'user.subscriptions.count' do
        post v1_users_subscriptions_path, headers: authenticated_header(user), params: {
            subscription: valid_params_two
        }
      end
      res =  parsed_response(response)['subscription']
      subscription = Subscription.find(res['id'])
      amount = (subscription.frequency_entity.dates(Date.tomorrow).count * subscription.vendor_product.price).to_f
      assert_equal 1, res['tokens'].count
      assert_equal amount, res['amount']
      assert_equal amount, res['total_amount']
      assert_equal 0, res['discount']
      assert_equal 0.0, res['wallet']
    end
  end

  test 'create on end of month' do
    Timecop.travel Date.today.end_of_month - 4.days do
      assert_difference 'user.subscriptions.count' do
        post v1_users_subscriptions_path, headers: authenticated_header(user), params: {
            subscription: valid_params_two
        }
      end
      res =  parsed_response(response)['subscription']
      subscription = Subscription.find(res['id'])
      amount = (subscription.frequency_entity.dates(Date.tomorrow).count * subscription.vendor_product.price).to_f
      n_amount = (subscription.frequency_entity.dates(Date.today.next_month.beginning_of_month).count * subscription.vendor_product.price).to_f
      amount = amount + n_amount
      assert_equal 2, res['tokens'].count
      assert_equal amount, res['amount']
      assert_equal amount, res['total_amount']
      assert_equal 0, res['discount']
      assert_equal 0.0, res['wallet']
    end
  end

  test 'invalid params' do
    assert_difference 'user.subscriptions.count', 0 do
      post v1_users_subscriptions_path, headers: authenticated_header(user), params: {
          subscription: invalid_params_one
      }
    end
    assert_equal 'Start on should be greater that today\'s date', parsed_response(response)['errors']

    assert_difference 'user.subscriptions.count', 0 do
      post v1_users_subscriptions_path, headers: authenticated_header(user), params: {
          subscription: invalid_params_two
      }
    end
    assert_equal 'Product cannot be delivered to your location. Kindly choose another product.', parsed_response(response)['errors']
  end

  test 'index' do
    get v1_users_subscriptions_path, headers: authenticated_header(user)
    assert_response :success
  end

  test 'delete draft' do
    assert_difference 'user.subscriptions.count', -1 do
      delete v1_users_subscription_path(id: subs.id), headers: authenticated_header(user)
    end

    #should not del others
    assert_difference 'user.subscriptions.count', 0 do
      delete v1_users_subscription_path(id: sub_two.id), headers: authenticated_header(user)
    end
  end

  private

  def user
    users(:jay)
  end

  def valid_params
    {
        start_on: Date.today.next_month.beginning_of_month,
        frequency: 'Every Day',
        vendor_product_id: vendor_products(:claire_product_two).id,
        user_location_id: user_locations(:jay_location_two).id
    }
  end

  def valid_params_two
    {
        start_on: Date.tomorrow,
        frequency: 'Every Day',
        vendor_product_id: vendor_products(:claire_product_two).id,
        user_location_id: user_locations(:jay_location_two).id
    }
  end

  def invalid_params_one
    {
        start_on: Date.today,
        frequency: 'Every Day',
        vendor_product_id: vendor_products(:claire_product_one).id,
        user_location_id: user_locations(:jay_location_two).id
    }
  end

  def invalid_params_two
    {
        start_on: Date.tomorrow,
        frequency: 'Every Day',
        vendor_product_id: vendor_products(:phil_product_one).id,
        user_location_id: user_locations(:jay_location_two).id
    }
  end

  def subs
    subscriptions(:jay_sub_two)
  end

  def sub_two
    subscriptions(:jay_sub_one)
  end

end
