require 'test_helper'

class Api::V1::Users::LocationsControllerTest < ActionDispatch::IntegrationTest
  test "get index" do
    get v1_users_locations_path, headers: authenticated_header(jay)
    assert_response :success
    assert_equal jay.locations.count, parsed_response(response)['locations'].count
    res_location = UserLocation.find parsed_response(response)['locations'].first['id']
    assert_equal 'Sony Nagar 3rd Street, Sony Nagar, Velachery, Zone 13 Adyar, Chennai, Chennai district, Tamil Nadu, 600042, India', res_location.geo_address
  end

  test 'create location' do
    assert_difference 'jay.locations.count' do
      post v1_users_locations_path, headers: authenticated_header(jay), params: {location: location_params}
      assert_response :success
    end
    res_location = jay.locations.most_recently_created
    location_params.each do |key, value|
      assert_equal res_location.send(key), value
    end
    assert_equal 'Lakshmi Hayagriva Nagar, Ward 177, Zone 13 Adyar, Chennai, Chennai district, Tamil Nadu, 6000042, India', res_location.geo_address
  end

  test 'create with un permitted params' do
    assert_difference 'jay.locations.count', 0 do
      post v1_users_locations_path, headers: authenticated_header(jay), params: {locationed: location_params}
      assert_response 422
    end
    res = parsed_response(response)['errors']
    assert_equal 'Location parameter is required', res
  end

  test 'show location' do
    get v1_users_location_path(id: location.id), headers: authenticated_header(jay)
    assert_response :success
    res_location = parsed_response(response)['location']
    res_location.each do |key, value|
      assert_equal location.reload.send(key), value
    end
  end

  test 'edit location' do
    patch v1_users_location_path(id: location.id), headers: authenticated_header(jay), params: {location: location_params}
    assert_response :success
    location_params.each do |key, value|
      assert_equal location.reload.send(key), value
    end
    assert_equal 'Lakshmi Hayagriva Nagar, Ward 177, Zone 13 Adyar, Chennai, Chennai district, Tamil Nadu, 6000042, India', location.reload.geo_address
  end

  private

  def jay
    users(:jay)
  end

  def location_params
    {
        address_line: 'No 98',
        latitude: 12.984417,
        longitude: 80.212176,
        locality: 'Felcon Street'
    }
  end

  def location
    user_locations(:jay_location)
  end

end
