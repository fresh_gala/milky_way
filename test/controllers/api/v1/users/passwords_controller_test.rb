require 'test_helper'

class Api::V1::Users::PasswordsControllerTest < ActionDispatch::IntegrationTest
  test "request forgot password and update password" do
    assert jay.encrypted_otp.nil?
    assert_difference 'SendOtpWorker.jobs.size', 1 do
      post v1_users_passwords_path, params: {account_identifier: '9367733300'}
    end
    assert_response 200
    user_id = parsed_response(response)['user']['user_id']
    jay.update(encrypted_otp: BCrypt::Password.create('123456'))
    assert jay.encrypted_otp.present?
    #invalid otp
    patch v1_users_passwords_path, params: {id: user_id, otp: '999999', password: 'password', password_confirmation: 'password'}
    res = parsed_response(response)['errors']
    assert_response 422
    assert res.present?
    assert_equal 'Otp is not valid', res
    #invalid password
    patch v1_users_passwords_path, params: {id: user_id, otp: '123456', password: 'passwords', password_confirmation: 'password'}
    res = parsed_response(response)['errors']
    assert_response 422
    assert res.present?
    assert_equal 'Password confirmation doesn\'t match Password', res
    #all correct
    patch v1_users_passwords_path, params: {id: user_id, otp: '123456', password: '1234567890', password_confirmation: '1234567890'}
    assert_response 200
    res = parsed_response(response)['message']
    assert_equal 'Password updated successfully and Login with your new password', res
    assert BCrypt::Password.new(jay.reload.encrypted_otp) == 'xxxxxx'
  end

  test 'unregistered user cannot access password' do
    post v1_users_passwords_path, params: {account_identifier: '9367733301'}
    res = parsed_response(response)['errors']
    assert_response 422
    assert_equal 'User email or mobile not found', res

    #valid user but not verified
    post v1_users_passwords_path, params: {account_identifier: gloria.mobile}
    res = parsed_response(response)['errors']
    assert_response 422
    assert_equal 'User email or mobile not found', res
  end

  private

  def jay
    users(:jay).reload
  end

  def gloria
    users(:gloria).reload
  end
end
