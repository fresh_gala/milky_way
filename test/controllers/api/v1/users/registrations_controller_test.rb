require 'test_helper'

class Api::V1::Users::RegistrationsControllerTest < ActionDispatch::IntegrationTest
  test "register new user" do
    assert_difference 'User.count' do
      post v1_users_registrations_path, params: {user: user_params}
      assert_response :success
      assert_equal 'User created successfully', parsed_response(response)['message']
    end
    user = User.most_recently_created
    assert_equal 'Tony', user.name
    assert_equal 'tony@starkenterprise.com', user.email
  end

  test "validate new user" do
    #invalid otp
    refute gloria.reload.verified?
    post v1_users_validate_user_path(id: gloria.id, otp: '999998')
    assert_response 422
    assert_equal 'Otp is not valid', parsed_response(response)['errors']
    refute gloria.reload.verified?
    # valid otp
    refute gloria.reload.verified?
    assert gloria.encrypted_otp.present?
    post v1_users_validate_user_path(id: gloria.id, otp: '999999')
    assert_response :success
    assert_equal 'Otp is valid.User registered successfully', parsed_response(response)['message']
    jwt_token = parsed_response(response)['jwt']
    assert jwt_token.present?
    assert gloria.reload.verified?
    assert BCrypt::Password.new(gloria.reload.encrypted_otp) == 'xxxxxx'
  end

  test "resend otp" do
    assert gloria.reload.valid_otp?('999999')
    get v1_users_resend_otp_path(id: gloria.id)
    refute gloria.reload.valid_otp?('999999')
  end

  private

  def user_params
    {
        name: 'Tony',
        email: 'tony@starkenterprise.com',
        password: 'password',
        password_confirmation: 'password',
        mobile: '9999999999'
    }
  end

  def gloria
    users(:gloria)
  end

end
