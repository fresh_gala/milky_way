require 'test_helper'

class Api::V1::Users::CashPaymentsControllerTest < ActionDispatch::IntegrationTest
  test "create a cash payment" do
    assert_difference 'user.subscriptions.count' do
      post v1_users_subscriptions_path, headers: authenticated_header(user), params: {
          subscription: subs_params
      }
    end
    res = parsed_response(response)['subscription']
    subscription = Subscription.find(res['id'])
    assert subscription.present?
    assert subscription.subscription_periods.unpaid.exists?
    Sidekiq::Testing.inline! do
      assert_difference 'user.cash_payments.count' do
        post v1_users_cash_payments_path, headers: authenticated_header(user), params: {
            cash_payment: {
                id: subscription.id,
                payment_to: 'Subscription',
                time_slot: '2pm to 6pm'
            }
        }
      end
    end
    assert_response :success
    res = parsed_response(response)['message']
    assert_equal 'Payment is successful', res
    refute subscription.reload.subscription_periods.unpaid.exists?
    cash_payment = user.cash_payments.most_recently_created
    assert_equal '2pm to 6pm', cash_payment.time_slot
    refute cash_payment.paid?
  end

  private

  def subs_params
    {
        start_on: Date.today.next_month.beginning_of_month,
        frequency: 'Every Day',
        vendor_product_id: vendor_products(:claire_product_two).id,
        user_location_id: user_locations(:jay_location_two).id
    }
  end

  def user
    users(:jay)
  end
end
