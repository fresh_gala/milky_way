require 'test_helper'

class Api::V1::Users::SubscriptionItemsControllerTest < ActionDispatch::IntegrationTest

  test "index" do
    get v1_users_subscription_items_path, headers: authenticated_header(user)
    assert_response :success
    res = parsed_response(response)['items']
    assert_equal 1, res.count

    get v1_users_subscription_items_path(date: Date.today), headers: authenticated_header(user)
    assert_response :success
    res = parsed_response(response)['items']
    assert_equal 1, res.count
  end

  test 'update' do
    #update for tomorrow
    Timecop.travel DateTime.now.beginning_of_month + 12.hours do
      patch v1_users_subscription_item_path(id: item.id, date: item.date), headers: authenticated_header(user),
            params: {
                item: {
                    paused: true
                }
            }
      assert_response :success
      assert item.paused
    end

    #update if before nyt 8'O clock
    Timecop.travel DateTime.now.beginning_of_month + 12.hours do
      patch v1_users_subscription_item_path(id: item_two.id, date: item_two.date),
            headers: authenticated_header(user),
            params: {
                item: {
                    paused: true
                }
            }
      assert_response :success
      assert item_two.paused
    end
    #should not update
    Timecop.travel DateTime.now.beginning_of_month + 20.hours + 1.minute do
      patch v1_users_subscription_item_path(id: item_two.id, date: item_two.date),
            headers: authenticated_header(user),
            params: {
                item: {
                    paused: false
                }
            }
      assert_response 422
      assert item_two.paused
    end
  end

  private

  def user
    users(:jay).reload
  end

  def item
    daily_usages(:jay_sub_one_2).reload
  end

  def item_two
    daily_usages(:jay_sub_one_1).reload
  end
end
