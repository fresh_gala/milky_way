require 'test_helper'

class Api::V1::Users::TransactionsControllerTest < ActionDispatch::IntegrationTest

  test "get index" do
    get v1_users_transactions_path, headers: authenticated_header(jay)
    assert_response :success
    res = parsed_response(response)['transaction']
    assert_equal jay.transactions.count, res.count
  end

  test 'get show' do
    get v1_users_transaction_path(transaction), headers: authenticated_header(jay)
    assert_response :success
    res = parsed_response(response)['transaction']
    res.each do |key, value|
      if key == 'date'
        assert_equal Date.parse(value), transaction.send(key).to_date
      elsif key == 'amount'
        assert_equal value, parse_money(transaction.amount)
      else
        assert_equal value, transaction.send(key)
      end
    end
  end

  # test 'create' do
  #   balance = jay.wallet.balance
  #   assert_difference 'jay.transactions.count' do
  #     post v1_users_transactions_path, headers: authenticated_header(jay), params: {transaction: transaction_params}
  #   end
  #   assert_response :success
  #   trans = jay.transactions.most_recently_created
  #   assert_equal balance + transaction_params[:amount], jay.reload.wallet.balance
  #   assert_equal transaction_params[:txn_source_id], trans.txn_source_id
  # end

  private

  def jay
    users(:jay)
  end

  def transaction
    user_transactions(:jay_txn_0)
  end

  def transaction_params
    {
        amount: 10_000,
        status: 'authorized',
        txn_source_id: 'pay_6X6jcHoHdRdy79'
    }
  end

end

