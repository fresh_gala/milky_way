require 'test_helper'

class Api::V1::Users::ProductsControllerTest < ActionDispatch::IntegrationTest
  test "index products" do
    get v1_users_products_path, headers: authenticated_header(jay), params: {
        location_id: jay_location.id
    }
    assert_response :success
    assert_equal 2, parsed_response(response)['product_categories'].size
    assert_equal 1, parsed_response(response)['product_categories'].first['products'].size
  end

  test 'show' do
    get v1_users_product_path(id: products(:fg_product).id), headers: authenticated_header(jay), params: {
        location_id: jay_location.id
    }
    assert_response :success
    res = parsed_response(response)['product']
    assert_equal 'Fresh Gala Milk', res['name']
    assert_equal 1, res['vendor_products'].count
  end

  private

  def jay
    users(:jay)
  end

  def jay_location
    user_locations(:jay_location_two)
  end
end
