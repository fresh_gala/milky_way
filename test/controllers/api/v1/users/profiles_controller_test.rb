require 'test_helper'

class Api::V1::Users::ProfilesControllerTest < ActionDispatch::IntegrationTest
  test "get user info" do
    get v1_users_profiles_path, headers: authenticated_header(jay)
    assert_response :success
  end

  test "update user info and get user info" do
    patch v1_users_profiles_path, headers: authenticated_header(jay), params: {
        user: user_params
    }
    get v1_users_profiles_path, headers: authenticated_header(jay)
    assert_response :success
    response_text = parsed_response(response)['user']
    assert_equal Date.parse('31/07/1994'), Date.parse(response_text['date_of_birth'])
    assert_equal user_params[:gender], response_text['gender']
    assert_equal user_params[:name], response_text['name']
  end

  test 'login only verified user' do
    #login
    post user_token_create_path({auth: {email: jay.email, password: "password"}})
    assert_response :success
    assert parsed_response(response)['jwt'].present?
    #wrong password
    post user_token_create_path({auth: {email: jay.email, password: "secret"}})
    assert_equal 'Password is not valid', parsed_response(response)['errors']
    assert_response 400
    # not verified user
    post user_token_create_path({auth: {email: gloria.email, password: "password"}})
    assert_response 400
    assert_equal 'User not verified. Verify with the OTP sent to your mobile', parsed_response(response)['errors']
  end

  private

  def jay
    users(:jay)
  end

  def gloria
    users(:gloria)
  end

  def user_params
    {
        date_of_birth: '31/07/1994',
        gender: 'Male',
        name: 'Sunil Aadithya'
    }
  end
end
