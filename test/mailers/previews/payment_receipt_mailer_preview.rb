# Preview all emails at http://localhost:3000/rails/mailers/payment_receipt_mailer
class PaymentReceiptMailerPreview < ActionMailer::Preview

  def send_receipt
    PaymentReceiptMailer.send_receipt(PaymentReceipt.first)
  end

end
