# Preview all emails at http://localhost:3000/rails/mailers/ticket_mailer
class TicketMailerPreview < ActionMailer::Preview
  def welcome_alert
    TicketMailer.welcome_alert(Ticket.first)
  end
end
