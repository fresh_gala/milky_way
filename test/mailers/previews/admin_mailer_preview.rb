# Preview all emails at http://localhost:3000/rails/mailers/admin_mailer
class AdminMailerPreview < ActionMailer::Preview

  def ticket_create_alert
    AdminMailer.ticket_create_alert(Ticket.first)
  end

end
