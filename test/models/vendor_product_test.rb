require 'test_helper'

class VendorProductTest < ActiveSupport::TestCase
  test 'products inside the coordinates' do
    #claire for jay
    products = VendorProduct.near_by_products(jay_location_one.coordinates)
    assert_equal 2, products.size
    assert_equal claire, products.first.vendor
    #claire should not be visible to jay
    products = VendorProduct.near_by_products(jay_location_two.coordinates)
    assert products.size.zero?
    #phil should be visible to jay
    products = VendorProduct.near_by_products(jay_loc_three.coordinates)
    assert_equal 2, products.size
    assert_equal phil, products.first.vendor
    #phil and haley will be visible to jay if he is anna nagar
    products = VendorProduct.near_by_products(jay_loc_four.coordinates)
    assert_equal 3, products.size
    assert phil.in? products.map(&:vendor)
    assert haley.in? products.map(&:vendor)
    #phil, haley and claire will be visible to jay
    products = VendorProduct.near_by_products(jay_loc_five.coordinates)
    assert_equal 5, products.size
    assert phil.in? products.map(&:vendor)
    assert haley.in? products.map(&:vendor)
    assert claire.in? products.map(&:vendor)
  end

  private

  def claire
    vendors(:claire)
  end

  def phil
    vendors(:phil)
  end

  def haley
    vendors(:phil)
  end

  def jay_location_one
    user_locations(:jay_location_two)
  end

  def jay_location_two
    user_locations(:jay_location_three)
  end

  def jay_loc_three
    user_locations(:jay_location_four)
  end

  def jay_loc_four
    user_locations(:jay_location_five)
  end

  def jay_loc_five
    user_locations(:jay_location_six)
  end
end
