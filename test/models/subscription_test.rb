require 'test_helper'

class SubscriptionTest < ActiveSupport::TestCase

  test "first subscription for jay" do
    #for this month he has to pay nothing
    Timecop.travel Date.today.beginning_of_month + 5.days do
      payment = sub_one.payment_entity.amount
      assert_equal 0, payment[:amount]
      travel_to Date.today.next_month.beginning_of_month do
        assert_difference [-> {sub_one.subscription_periods.count}, -> {PaymentToken.count}] do
          sub_one.create_period(Date.today.beginning_of_month)
        end
        assert_difference [-> {sub_one.subscription_periods.count}, -> {PaymentToken.count}], 0 do
          sub_one.create_period(Date.today.beginning_of_month)
        end
        amount = sub_one.frequency_entity.dates(Date.today.beginning_of_month).count * sub_one.vendor_product.price
        payment = sub_one.payment_entity.amount
        assert_equal amount.to_f, payment[:amount]
      end
    end
    travel_to Date.today.beginning_of_month + 15.days do
      payment = sub_two.payment_entity.amount
      assert_equal 1092.0, payment[:amount].to_f
    end
  end

  private

  def user
    users(:jay).reload
  end

  def sub_one
    subscriptions(:jay_sub_one).reload
  end

  def sub_two
    subscriptions(:jay_sub_two).reload
  end

end
