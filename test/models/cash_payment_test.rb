require 'test_helper'

class CashPaymentTest < ActiveSupport::TestCase
  test "request cash payment" do
    Timecop.travel Date.today.next_month.beginning_of_month do
      assert_difference [-> {sub_one.subscription_periods.count}, -> {PaymentToken.count}] do
        sub_one.create_period(Date.today.beginning_of_month)
      end
      assert sub_one.subscription_periods.unpaid.exists?
      assert_difference [-> {user.cash_payments.count}, -> {user.payments.count}] do
        CashPayment.create(user, '10 am to 2pm', sub_one)
      end
      assert user.cash_payments.unpaid.exists?
      cash_payment = user.cash_payments.most_recently_created
      refute cash_payment.paid?
      cash_payment.paid!
      assert cash_payment.reload.paid?
      refute sub_one.subscription_periods.unpaid.exists?
    end
  end

  test 'should not create' do
    Timecop.travel Date.today.next_month.beginning_of_month do
      assert_difference [-> {sub_one.subscription_periods.count}, -> {PaymentToken.count}] do
        sub_one.create_period(Date.today.beginning_of_month)
      end
      assert sub_one.subscription_periods.unpaid.exists?
      cp = assert_difference -> {user.cash_payments.count} => 0, -> {user.payments.count} => 0 do
        CashPayment.create(user, nil, sub_one)
      end
      assert_equal 'Time slot can\'t be blank', cp.errors.full_messages.join(', ')
    end
  end

  private

  def user
    users(:jay).reload
  end

  def sub_one
    subscriptions(:jay_sub_one).reload
  end

  def sub_two
    subscriptions(:jay_sub_two).reload
  end
end
