ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'
require 'sidekiq/testing'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all
  include Devise::Test::IntegrationHelpers

  # Add more helper methods to be used by all tests here...
  #

  def parsed_response(response)
    JSON.parse(response.body)
  end

  def authenticated_header(user)
    token = Knock::AuthToken.new(payload: { sub: user.id }).token
    {
        'Authorization': "Bearer #{token}"
    }
  end

  def parse_money(amount)
    ApplicationController.helpers.humanized_money_with_symbol(amount)
  end
end
