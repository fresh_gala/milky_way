source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.1'

gem 'rails', '~> 5.2.0'
gem 'pg', '>= 0.18', '< 2.0'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'
gem 'bootsnap', '>= 1.1.0', require: false
gem 'rack-cors'
gem 'bcrypt'
gem 'devise'
gem 'sidekiq'
gem 'redis'
gem 'knock'
gem 'geocoder'
gem 'paper_trail'
gem 'razorpay'
gem 'money-rails'
gem 'haml-rails'
gem 'simple_form'
gem 'activerecord-import'
gem 'font-awesome-sass'
gem 'toastr-rails'
gem 'jquery-rails'
gem 'aws-sdk-s3', require: false
gem 'mini_magick'
gem 'premailer-rails'
gem 'wicked_pdf'
gem 'wkhtmltopdf-binary', '~> 0.12.3.1'
gem 'combine_pdf'
gem 'serviceworker-rails'

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'pry'
  gem 'pry-rails'
  gem 'puma', '~> 3.11'
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :production, :development do
  # Deployments
  gem 'capistrano'
  gem 'capistrano-bundler', require: false
  gem 'capistrano-passenger'
  # For rails support
  gem 'capistrano-rails', require: false
  gem 'capistrano-rails-collection'
  # For rvm support
  gem 'capistrano-rvm'
  # Background Worker
  gem 'capistrano-sidekiq'
  # for yarn support
  gem 'capistrano-yarn'
  gem 'rack-mini-profiler', require: false
  gem 'capistrano-rake', require: false
  gem 'capistrano-rails-tail-log', require: false
  gem 'whenever', require: false
end

group :production do
  gem 'passenger'
end

group :staging do
  gem 'puma', '~> 3.11'
end

group :test do
  gem 'capybara', '>= 2.15', '< 4.0'
  gem 'selenium-webdriver'
  gem 'chromedriver-helper'
  gem 'timecop'
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
