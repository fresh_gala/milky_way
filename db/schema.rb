# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_02_16_041344) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "active_storage_attachments", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.uuid "record_id", null: false
    t.uuid "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "admins", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
  end

  create_table "cart_items", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "cart_id"
    t.float "quantity"
    t.uuid "vendor_product_id"
    t.uuid "product_id"
    t.integer "price_in_paisa", default: 0, null: false
    t.string "price_currency", default: "INR", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cart_id"], name: "index_cart_items_on_cart_id"
    t.index ["product_id"], name: "index_cart_items_on_product_id"
    t.index ["vendor_product_id"], name: "index_cart_items_on_vendor_product_id"
  end

  create_table "carts", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_carts_on_user_id"
  end

  create_table "cash_payments", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.uuid "payment_id"
    t.date "date"
    t.string "time_slot"
    t.boolean "paid", default: false
    t.datetime "paid_at"
    t.date "due_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "amount_in_paisa", default: 0, null: false
    t.string "amount_currency", default: "INR", null: false
    t.index ["payment_id"], name: "index_cash_payments_on_payment_id"
    t.index ["user_id"], name: "index_cash_payments_on_user_id"
  end

  create_table "daily_usages", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "subscription_period_id"
    t.float "quantity", default: 0.0
    t.integer "amount_in_paisa", default: 0, null: false
    t.string "amount_currency", default: "INR", null: false
    t.date "date"
    t.boolean "paused", default: false
    t.text "description"
    t.boolean "delivered", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["subscription_period_id"], name: "index_daily_usages_on_subscription_period_id"
  end

  create_table "payment_receipt_items", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "payment_receipt_id"
    t.string "parent_type"
    t.uuid "parent_id"
    t.integer "quantity", default: 0
    t.integer "rate_in_paisa", default: 0, null: false
    t.string "rate_currency", default: "INR", null: false
    t.integer "amount_in_paisa", default: 0, null: false
    t.string "amount_currency", default: "INR", null: false
    t.string "description"
    t.string "unit_of_measurement"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["parent_type", "parent_id"], name: "index_payment_receipt_items_on_parent_type_and_parent_id"
    t.index ["payment_receipt_id"], name: "index_payment_receipt_items_on_payment_receipt_id"
  end

  create_table "payment_receipts", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.uuid "vendor_id"
    t.uuid "vendor_location_id"
    t.uuid "user_location_id"
    t.uuid "payment_id"
    t.string "user_location_text", default: [], array: true
    t.string "vendor_location_text", default: [], array: true
    t.string "receipt_number"
    t.datetime "date"
    t.string "paid_through"
    t.integer "wallet_in_paisa", default: 0, null: false
    t.string "wallet_currency", default: "INR", null: false
    t.integer "discount_in_paisa", default: 0, null: false
    t.string "discount_currency", default: "INR", null: false
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["payment_id"], name: "index_payment_receipts_on_payment_id"
    t.index ["user_id"], name: "index_payment_receipts_on_user_id"
    t.index ["user_location_id"], name: "index_payment_receipts_on_user_location_id"
    t.index ["vendor_id"], name: "index_payment_receipts_on_vendor_id"
    t.index ["vendor_location_id"], name: "index_payment_receipts_on_vendor_location_id"
  end

  create_table "payment_tokens", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.uuid "payment_id"
    t.string "token"
    t.string "parent_type"
    t.uuid "parent_id"
    t.boolean "paid", default: false
    t.datetime "paid_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "payment_type"
    t.index ["parent_type", "parent_id"], name: "index_payment_tokens_on_parent_type_and_parent_id"
    t.index ["payment_id"], name: "index_payment_tokens_on_payment_id"
    t.index ["token", "user_id"], name: "index_payment_tokens_on_token_and_user_id", unique: true
    t.index ["user_id"], name: "index_payment_tokens_on_user_id"
  end

  create_table "payments", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.integer "amount_in_paisa", default: 0, null: false
    t.string "amount_currency", default: "INR", null: false
    t.string "source_id"
    t.jsonb "source_document"
    t.datetime "date"
    t.string "paid_through"
    t.string "status"
    t.boolean "captured", default: false
    t.date "captured_at"
    t.boolean "refunded", default: false
    t.date "refunded_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "wallet_in_paisa", default: 0, null: false
    t.string "wallet_currency", default: "INR", null: false
    t.integer "discount_in_paisa", default: 0, null: false
    t.string "discount_currency", default: "INR", null: false
    t.integer "total_in_paisa", default: 0, null: false
    t.string "total_currency", default: "INR", null: false
    t.index ["user_id"], name: "index_payments_on_user_id"
  end

  create_table "product_categories", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "products", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.string "unit_of_measurement"
    t.integer "base_price_in_paisa", default: 0, null: false
    t.string "base_price_currency", default: "INR", null: false
    t.uuid "product_category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "freshgala_products", default: false
    t.boolean "sample", default: false
    t.index ["product_category_id"], name: "index_products_on_product_category_id"
  end

  create_table "subscription_periods", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "subscription_id"
    t.integer "amount_in_paisa", default: 0, null: false
    t.string "amount_currency", default: "INR", null: false
    t.integer "discount_amount_in_paisa", default: 0, null: false
    t.string "discount_amount_currency", default: "INR", null: false
    t.boolean "paid", default: false
    t.datetime "paid_at"
    t.date "month"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "wallet_in_paisa", default: 0, null: false
    t.string "wallet_currency", default: "INR", null: false
    t.index ["subscription_id"], name: "index_subscription_periods_on_subscription_id"
  end

  create_table "subscription_requests", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "mobile"
    t.text "address"
    t.string "product"
    t.float "quantity"
    t.string "status", default: "PENDING"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "subscriptions", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.date "start_on"
    t.date "end_on"
    t.string "frequency"
    t.uuid "vendor_product_id"
    t.float "quantity", default: 0.0
    t.uuid "user_location_id"
    t.boolean "draft", default: true
    t.boolean "active", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_subscriptions_on_user_id"
    t.index ["user_location_id"], name: "index_subscriptions_on_user_location_id"
    t.index ["vendor_product_id"], name: "index_subscriptions_on_vendor_product_id"
  end

  create_table "tickets", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "locality"
    t.string "mobile"
    t.string "status"
    t.text "message"
    t.datetime "delivery_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_locations", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.string "address_line"
    t.string "locality"
    t.float "latitude"
    t.float "longitude"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["latitude", "longitude"], name: "index_user_locations_on_latitude_and_longitude"
    t.index ["user_id"], name: "index_user_locations_on_user_id"
  end

  create_table "user_transactions", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.uuid "wallet_id"
    t.string "txn_type"
    t.string "status"
    t.integer "amount_in_paisa", default: 0, null: false
    t.string "amount_currency", default: "INR", null: false
    t.boolean "refunded", default: false
    t.date "refunded_at"
    t.datetime "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "description"
    t.index ["user_id"], name: "index_user_transactions_on_user_id"
    t.index ["wallet_id"], name: "index_user_transactions_on_wallet_id"
  end

  create_table "users", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "password_digest"
    t.string "mobile"
    t.string "encrypted_otp"
    t.datetime "opt_expires_in"
    t.boolean "verified", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "date_of_birth"
    t.string "gender"
    t.boolean "needy", default: true
  end

  create_table "vendor_locations", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "vendor_id"
    t.float "latitude"
    t.float "longitude"
    t.string "address_line_1"
    t.string "address_line_2"
    t.string "locality"
    t.float "service_radius", default: 0.0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["latitude"], name: "index_vendor_locations_on_latitude"
    t.index ["longitude"], name: "index_vendor_locations_on_longitude"
    t.index ["service_radius"], name: "index_vendor_locations_on_service_radius"
    t.index ["vendor_id"], name: "index_vendor_locations_on_vendor_id"
  end

  create_table "vendor_products", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "vendor_id"
    t.uuid "product_id"
    t.integer "price_in_paisa", default: 0, null: false
    t.string "price_currency", default: "INR", null: false
    t.decimal "min_quantity"
    t.decimal "max_quantity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_vendor_products_on_product_id"
    t.index ["vendor_id"], name: "index_vendor_products_on_vendor_id"
  end

  create_table "vendors", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "mobile"
    t.string "secondary_contact"
    t.boolean "active", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.string "encrypted_password", default: "", null: false
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "encrypted_otp"
    t.datetime "opt_expires_in"
    t.datetime "remember_created_at"
    t.string "remember_token"
    t.index ["email"], name: "index_vendors_on_email", unique: true
    t.index ["mobile"], name: "index_vendors_on_mobile", unique: true
  end

  create_table "versions", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "item_type", null: false
    t.uuid "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  create_table "wallets", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "currency", default: "INR"
    t.uuid "user_id"
    t.float "limit", default: 5000.0
    t.integer "balance_in_paisa", default: 0, null: false
    t.string "balance_currency", default: "INR", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_wallets_on_user_id"
  end

  add_foreign_key "cart_items", "carts"
  add_foreign_key "cart_items", "products"
  add_foreign_key "cart_items", "vendor_products"
  add_foreign_key "carts", "users"
  add_foreign_key "cash_payments", "payments"
  add_foreign_key "cash_payments", "users"
  add_foreign_key "daily_usages", "subscription_periods"
  add_foreign_key "payment_receipt_items", "payment_receipts"
  add_foreign_key "payment_receipts", "payments"
  add_foreign_key "payment_receipts", "user_locations"
  add_foreign_key "payment_receipts", "users"
  add_foreign_key "payment_receipts", "vendor_locations"
  add_foreign_key "payment_receipts", "vendors"
  add_foreign_key "payment_tokens", "payments"
  add_foreign_key "payment_tokens", "users"
  add_foreign_key "payments", "users"
  add_foreign_key "products", "product_categories"
  add_foreign_key "subscription_periods", "subscriptions"
  add_foreign_key "subscriptions", "user_locations"
  add_foreign_key "subscriptions", "users"
  add_foreign_key "subscriptions", "vendor_products"
  add_foreign_key "user_locations", "users"
  add_foreign_key "user_transactions", "users"
  add_foreign_key "user_transactions", "wallets"
  add_foreign_key "vendor_locations", "vendors"
  add_foreign_key "vendor_products", "products"
  add_foreign_key "vendor_products", "vendors"
  add_foreign_key "wallets", "users"
end
