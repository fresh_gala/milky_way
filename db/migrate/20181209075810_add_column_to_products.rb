class AddColumnToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :freshgala_products, :boolean, default: false
    add_column :products, :subscription, :boolean, default: false
  end
end
