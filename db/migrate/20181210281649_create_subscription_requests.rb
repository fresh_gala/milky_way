class CreateSubscriptionRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :subscription_requests do |t|
      t.string :name
      t.string :email
      t.string :mobile
      t.text :address
      t.string :product
      t.float :quantity
      t.string :status, default: 'PENDING'

      t.timestamps
    end
  end
end
