class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users, id: :uuid do |t|
      t.string :name
      t.string :email
      t.string :password_digest
      t.string :mobile
      t.string :encrypted_otp
      t.datetime :opt_expires_in
      t.boolean :verified, default: false
      t.timestamps
    end
  end
end
