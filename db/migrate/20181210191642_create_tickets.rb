class CreateTickets < ActiveRecord::Migration[5.1]
  def change
    create_table :tickets do |t|
      t.string :name
      t.string :email
      t.string :locality
      t.string :mobile
      t.string :status
      t.text :message
      t.datetime :delivery_date

      t.timestamps
    end
  end
end
