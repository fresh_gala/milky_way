class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products, id: :uuid do |t|
      t.string :name
      t.text :description
      t.string :unit_of_measurement
      t.monetize :base_price
      t.references :product_category, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
