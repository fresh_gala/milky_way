class CreateVendorProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :vendor_products, id: :uuid do |t|
      t.references :vendor, foreign_key: true, type: :uuid
      t.references :product, foreign_key: true, type: :uuid
      t.monetize :price
      t.decimal :min_quantity
      t.decimal :max_quantity

      t.timestamps
    end
  end
end
