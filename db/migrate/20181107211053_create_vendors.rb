class CreateVendors < ActiveRecord::Migration[5.2]
  def change
    create_table :vendors, id: :uuid do |t|
      t.string :name
      t.string :email
      t.string :primary_contact
      t.string :secondary_contact
      t.string :password_digest
      t.boolean :active, default: true

      t.timestamps
    end
  end
end
