class CreatePaymentReceiptItems < ActiveRecord::Migration[5.2]
  def change
    create_table :payment_receipt_items, id: :uuid do |t|
      t.references :payment_receipt, foreign_key: true, type: :uuid
      t.references :parent, polymorphic: true, type: :uuid
      t.integer :quantity, default: 0
      t.monetize :rate
      t.monetize :amount
      t.string :description
      t.string :unit_of_measurement
      t.timestamps
    end
  end
end
