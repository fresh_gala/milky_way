class AddColumnsToPayments < ActiveRecord::Migration[5.2]
  def change
    add_monetize :payments, :wallet
    add_monetize :payments, :discount
    add_monetize :payments, :total
  end
end
