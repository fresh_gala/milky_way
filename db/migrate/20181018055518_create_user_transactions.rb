class CreateUserTransactions < ActiveRecord::Migration[5.2]
  def change
    create_table :user_transactions, id: :uuid do |t|
      t.references :user, foreign_key: true, type: :uuid
      t.references :wallet, foreign_key: true, type: :uuid
      t.references :payment, foreign_key: true, type: :uuid
      t.string :txn_type
      t.string :status
      t.monetize :amount
      t.boolean :refunded, default: false
      t.date :refunded_at
      t.datetime :date

      t.timestamps
    end
  end
end
