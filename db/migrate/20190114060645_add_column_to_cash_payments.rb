class AddColumnToCashPayments < ActiveRecord::Migration[5.2]
  def change
    add_monetize :cash_payments, :amount
  end
end
