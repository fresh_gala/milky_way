class CreateVendorLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :vendor_locations, id: :uuid do |t|
      t.references :vendor, foreign_key: true, type: :uuid
      t.float :latitude, index: true
      t.float :longitude, index: true
      t.string :address_line_1
      t.string :address_line_2
      t.string :locality
      t.float :service_radius, default: 0.0, index: true

      t.timestamps
    end
  end
end
