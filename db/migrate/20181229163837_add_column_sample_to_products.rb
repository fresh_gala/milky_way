class AddColumnSampleToProducts < ActiveRecord::Migration[5.2]
  def change
    remove_column :products, :subscription, :boolean
    add_column :products, :sample, :boolean, default: false
  end
end
