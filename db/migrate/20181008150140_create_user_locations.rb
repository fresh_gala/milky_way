class CreateUserLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :user_locations, id: :uuid do |t|
      t.references :user, foreign_key: true, type: :uuid
      t.string :address_line
      t.string :locality


      t.float :latitude
      t.float :longitude

      t.timestamps
    end
    add_index :user_locations, [:latitude, :longitude]
  end
end
