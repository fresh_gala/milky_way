class CreateCashPayments < ActiveRecord::Migration[5.2]
  def change
    create_table :cash_payments, id: :uuid do |t|
      t.references :user, foreign_key: true, type: :uuid
      t.references :payment, foreign_key: true, type: :uuid
      t.date :date
      t.string :time_slot
      t.boolean :paid, default: false
      t.datetime :paid_at
      t.date :due_date

      t.timestamps
    end
  end
end
