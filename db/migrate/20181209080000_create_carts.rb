class CreateCarts < ActiveRecord::Migration[5.2]
  def change
    create_table :carts, id: :uuid do |t|
      t.references :user, foreign_key: true, type: :uuid
      t.timestamps
    end
    reversible do |dir|
      dir.up do
        User.find_each do |user|
          user.create_cart!
        end
      end
    end
  end
end
