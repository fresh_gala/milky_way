class CreatePaymentReceipts < ActiveRecord::Migration[5.2]
  def change
    create_table :payment_receipts, id: :uuid do |t|
      t.references :user, foreign_key: true, type: :uuid
      t.references :vendor, foreign_key: true, type: :uuid
      t.references :vendor_location, foreign_key: true, type: :uuid
      t.references :user_location, foreign_key: true, type: :uuid
      t.references :payment, foreign_key: true, type: :uuid
      t.string :user_location_text, array: true, default: []
      t.string :vendor_location_text, array: true, default: []
      t.string :receipt_number
      t.datetime :date
      t.string :paid_through
      t.monetize :wallet
      t.monetize :discount
      t.string :status

      t.timestamps
    end
  end
end
