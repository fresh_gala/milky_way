class AddColumnToPaymentToken < ActiveRecord::Migration[5.2]
  def change
    add_column :payment_tokens, :payment_type, :string
  end
end
