class CreateCartItems < ActiveRecord::Migration[5.2]
  def change
    create_table :cart_items, id: :uuid do |t|
      t.references :cart, foreign_key: true, type: :uuid
      t.float :quantity
      t.references :vendor_product, foreign_key: true, type: :uuid
      t.references :product, foreign_key: true, type: :uuid
      t.monetize :price

      t.timestamps
    end
  end
end
