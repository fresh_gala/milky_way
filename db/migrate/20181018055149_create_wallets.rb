class CreateWallets < ActiveRecord::Migration[5.2]
  def change
    create_table :wallets, id: :uuid do |t|
      t.string :currency, default: 'INR'
      t.references :user, foreign_key: true, type: :uuid
      t.float :limit, default: 5000
      t.monetize :balance

      t.timestamps
    end
    reversible do |dir|
      dir.up do
        User.find_each do |user|
          user.create_wallet!
        end
      end
    end
  end
end
