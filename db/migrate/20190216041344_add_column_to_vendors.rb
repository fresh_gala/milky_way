class AddColumnToVendors < ActiveRecord::Migration[5.2]
  def change
    remove_column :vendors, :password_digest, :string
    change_column_default :vendors, :active, from: true, to: false
    add_index :vendors, :email, unique: true
    add_column :vendors, :sign_in_count, :integer , default: 0, null: false
    add_column :vendors, :current_sign_in_at, :datetime
    add_column :vendors, :encrypted_password, :string, null: false, default: ''
    add_column :vendors, :last_sign_in_at, :datetime
    add_column :vendors, :current_sign_in_ip, :inet
    add_column :vendors, :last_sign_in_ip, :inet
    rename_column :vendors, :primary_contact, :mobile
    add_index :vendors, :mobile, unique: true
    add_column :vendors, :encrypted_otp, :string
    add_column :vendors, :opt_expires_in, :datetime
    add_column :vendors, :remember_created_at, :datetime
    add_column :vendors, :remember_token, :string
  end
end
