class CreateSubscriptionPeriods < ActiveRecord::Migration[5.2]
  def change
    create_table :subscription_periods, id: :uuid do |t|
      t.references :subscription, foreign_key: true, type: :uuid
      t.monetize :amount
      t.monetize :discount_amount
      t.boolean :paid, default: false
      t.datetime :paid_at
      t.date :month

      t.timestamps
    end
  end
end
