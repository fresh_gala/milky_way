class AddColumnsToUserTransaction < ActiveRecord::Migration[5.2]
  def change
    remove_reference :user_transactions, :payment, type: :uuid
    add_column :user_transactions, :description, :string
  end
end
