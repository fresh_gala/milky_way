class CreateDailyUsages < ActiveRecord::Migration[5.2]
  def change
    create_table :daily_usages, id: :uuid do |t|
      t.references :subscription_period, foreign_key: true, type: :uuid
      t.float :quantity, default: 0.0
      t.monetize :amount
      t.date :date
      t.boolean :paused, default: false
      t.text :description
      t.boolean :delivered, default: false
      t.timestamps
    end
  end
end
