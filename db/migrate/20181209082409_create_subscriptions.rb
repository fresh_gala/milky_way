class CreateSubscriptions < ActiveRecord::Migration[5.2]
  def change
    create_table :subscriptions, id: :uuid do |t|
      t.references :user, foreign_key: true, type: :uuid
      t.date :start_on
      t.date :end_on
      t.string :frequency
      t.references :vendor_product, foreign_key: true, type: :uuid
      t.float :quantity, default: 0.0
      t.references :user_location, foreign_key: true, type: :uuid

      t.boolean :draft, default: true
      t.boolean :active, default: false

      t.timestamps
    end
  end
end
