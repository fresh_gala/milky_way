class CreatePaymentTokens < ActiveRecord::Migration[5.2]
  def change
    create_table :payment_tokens, id: :uuid do |t|
      t.references :user, foreign_key: true, type: :uuid
      t.references :payment, foreign_key: true, type: :uuid
      t.string :token
      t.references :parent, polymorphic: true, type: :uuid
      t.boolean :paid, default: false
      t.datetime :paid_at
      t.timestamps
    end
    add_index :payment_tokens, ['token', 'user_id'], unique: true
  end
end
