class CreatePayments < ActiveRecord::Migration[5.2]
  def change
    create_table :payments, id: :uuid do |t|
      t.references :user, foreign_key: true, type: :uuid
      t.monetize :amount
      t.string :source_id
      t.jsonb :source_document
      t.datetime :date
      t.string :paid_through
      t.string :status
      t.boolean :captured, default: false
      t.date :captured_at
      t.boolean :refunded, default: false
      t.date :refunded_at

      t.timestamps
    end
    add_index :payments, [:source_id, :paid_through], unique: true
  end
end
