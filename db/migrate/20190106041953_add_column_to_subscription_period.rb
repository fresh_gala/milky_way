class AddColumnToSubscriptionPeriod < ActiveRecord::Migration[5.2]
  def change
    add_monetize :subscription_periods, :wallet
  end
end
