class RemoveIndexFromPayments < ActiveRecord::Migration[5.2]
  def change
    remove_index :payments, column: [:source_id, :paid_through]
  end
end
